xquery version "1.0";

(: Get all the pending updates from the pending updates file and run them on the right EAD files :)

import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style="http://danmccreary.com/style" at "../modules/style.xqm";
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

let $title := 'Run EAD Updates'

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $data-collection := $config:ead-data-collection

let $pending-updates-file := $config:ead-update-file-path

let $updates := doc($pending-updates-file)//update
let $update-count := count($updates)


return
   if ($update-count lt 1)
      then <error><message>No Updates Pending at {$pending-updates-file}</message></error>
      else (: continue :)
      
let $run-updates :=
  for $update in $updates
  let $run-update := ead:run-update($update)
  return
     <result>
       <update>{$update}</update>
       <run>{$run-update}</run>
     </result>

let $archive-file-path :=
  if ($debug)
    then ()
    else
let $archive :=  loc:archive-pending-updates('ead')
return $archive/archive-file-path/text()

return
if ($debug)
  then
    <results>
        {$run-updates}
    </results>
else
let $content :=
<div class="content">
  Found {count($updates)} update records done in collection {$data-collection}<br/>
  Success Count = { count($run-updates/run[contains(. , 'Success')]) }
  <br/>
  An archive of the updates has been placed at:
  <a href="{$config:web-base}/rest{$archive-file-path}">{$config:web-base}/rest{$archive-file-path}</a>
</div>
return style:assemble-page($title, $content)