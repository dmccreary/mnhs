xquery version "1.0";

(: test of HTTP GET with two parameters :)

let $current-dateTime := current-dateTime()
let $p1 := request:get-parameter('p1', '')
let $p2 := request:get-parameter('p2', '')

return
<results>
   <current-dateTime>{$current-dateTime}</current-dateTime>
   <p1>{$p1}</p1>
   <p2>{$p2}</p2>
</results>