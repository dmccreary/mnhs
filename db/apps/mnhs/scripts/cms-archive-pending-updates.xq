xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';


(: Copy the updates into an archive location and replace the current one :)

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $title := 'CMS Archive Pending Updates'

let $update-count := count($config:cms-updates)

let $content :=
<div class="content">
   {if ($update-count gt 0)
      then
        let $run-archive := loc:archive-pending-updates('cms')
        (:
           <archive-result>
                <user>{xmldb:get-current-user()}</user>
                <archive-file-path>{$archive-file-path}</archive-file-path>
                <update-count>{$update-count}</update-count>
             </archive-result>
             :)
        return
           concat('The pending updates file with ', $run-archive/update-count, ' items has been archived at ', $run-archive/archive-file-path)
       else
          'No update are pending - nothing to archive'
   }
</div>

return style:assemble-page($title, $content)