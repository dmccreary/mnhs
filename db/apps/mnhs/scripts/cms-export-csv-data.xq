xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

let $title := 'Export CMS Update Data To File System' 
let $format := request:get-parameter('format', 'csv')

(: this will be \ on windows and / on UNIX :)
let $file-separator := util:system-property('file.separator')

(: get all the updates :)
let $updates := $config:cms-updates

(: http://exist-db.org/exist/apps/fundocs/view.html?uri=http://exist-db.org/xquery/file :)

let $export-dir := request:get-parameter('export-dir', $config:cms-export-file-path)

let $create-dirs-if-not-exists :=
   if (file:is-directory($export-dir))
      then ()
      else file:mkdirs($export-dir)

let $table :=
<table>
   <tr>
      <th>#</th>
      <th>Org Name</th>
      <th>Authority Name</th>
      <th>Authority ID</th>
      <th>Nr Matches</th>
      <th>ID Match</th>
   </tr>
        {for $update at $count in $updates
           let $irn := $update/irn/text()
           let $cms-record := cms:get-tuple($irn)
             return
                <tr>
                    <td>{$count}</td>
                    <td>{$update/irn/text()}</td>
                    <td>{$cms-record/atom[@name='SummaryData']/text()}</td>
                    <td>{$cms-record/atom[@name='AutAuthorityID']/text()}</td>
                    <td>{$update/cmd/text()}</td>
                    <td>{$update/locid/text()}</td>
                    <td>{$update/newname/text()}</td>
                 </tr>
        }

   </table>

let $content :=
<div class="content">
   {$table}
</div>

return
  if ($format = 'csv')
  then
    (: this is a single string :)
    let $csv-data := util2:table-to-csv($table)
    let $csv-binary := util:base64-encode($csv-data)
    let $csv-xml := <root>{$csv-data}</root>
    (: file:serialize($node-set as node()*, $path as item(), $parameters as xs:string*)  :)
    let $store-xml := file:serialize($csv-xml, $config:cms-export-file-path, ())
    return
       file:serialize-binary($csv-binary, 'c:/temp/binary.txt')
  else
    style:assemble-page($title, $content)