xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';


(: Get a list of update command from a table-based form that has checkboxes in a update column 
   Incomming data must be in request:get-parameter-names() 
   note that other URL parameters that do not trigger updates should be removed
   examples: start, num and debug:)

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $title := 'CMS Add Pending Updates'

(: we assume each checkbox has a hidden field that has the id followed by a dash and the new name to be used 
<input type="checkbox" name="{$irn-number}|{$cmd}|{$new-locid}|{$new-name}"/>,
:)
let $update-list :=
<update-list>
   <query>{request:get-uri()}</query>
   <desc>The following items have been added to the pending-updates.xml file in {$config:ead-update-file-path}</desc>
   <updates>
      {for $name in request:get-parameter-names()
        return
        (: we throw out the URL parameter names that are not real update requests :)
          if ($name = 'start' or $name = 'num' or $name = 'debug')
             then ()
             else (: continue since we have a real update to do :)
                (: convert pipe delimited strings into XML update nodes :)
                let $update-node := cms:update-string-to-xml($name)
                (: append the node to the end of the pending-updates.xml file :)
                let $run-append :=
                   if ($update-node)
                      then cms:append-update-to-pending-updates-file($update-node)   
                      else () 
                return
                  $update-node
      }
   </updates>
</update-list>

(: <form method="post" action="cms-run-updates.xq"> :)

let $content :=
<div class="content">
   The following items have been added to the pending updates list.
   
       <table class="table table-striped table-bordered table-hover table-condensed">
          <thead>
             <tr>
                <th>#</th>
                <th>IRN</th>
                <th>Command</th>
                <th>Current Name</th>
                <th>Current Authority ID</th>
                <th>New Name</th>
                <th>New ID</th>
             </tr>
          </thead>
         {for $update at $count in $update-list/updates/update
         
            (: <update>
                <input-string>{$input}</input-string>
                <irn></irn>
                <cmd></cmd>
                <new-id></new-id>
                <new-name></new-name>
             </update>
            :)
            let $irn := $update/irn/text()
            let $tuple := cms:get-tuple($irn)
            let $current-id := $tuple/atom[@name='AutAuthorityID']/text()
            let $current-name := cms:get-name-from-tuple($tuple)
            return
               <tr>
                 <th>{$count}</th>
                 <td>{$update/irn/text()}</td>
                 <td>{$update/cmd/text()}</td>
                 <td>{$current-name}</td>
                 <td>{$current-id}</td>
                 <td>{$update/new-name/text()}</td>
                 <td>{$update/new-id/text()}</td>
               </tr>
         }
       </table>
       <!-- <button type="submit">Run ALL EAD Updates</button></form> -->
       <a href="../views/cms-list-pending-updates.xq">View All Pending Updates (HTML)</a>
   
</div>

return if ($debug)
  then $update-list
  else style:assemble-page($title, $content)