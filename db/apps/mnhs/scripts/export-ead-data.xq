xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

let $title := 'Export EAD Data'

(: this will be \ on windows and / on UNIX :)
let $file-separator := util:system-property('file.separator')

(: http://exist-db.org/exist/apps/fundocs/view.html?uri=http://exist-db.org/xquery/file :)

let $files :=
  for $child in xmldb:get-child-resources($config:ead-data)
  order by $child
  return
     <file>{$child}</file>

let $export-dir := request:get-parameter('export-dir', $config:ead-export-dir)

let $create-dirs-if-not-exists :=
   if (file:is-directory($export-dir))
      then ()
      else file:mkdirs($export-dir)

let $content :=
<div class="content">
   <div class="row col-md-12">
   The following files have been exported into the export directory.
   </div>
   <div class="row col-md-7">
   <table class="table table-striped table-bordered table-hover table-condensed ">
     <thead>
        <tr>
           <th>File Path</th>
           <th>Save OK</th>
        </tr>
     </thead>
     <tbody>
        {for $file-name in $files
             let $db-file-path := concat($config:ead-data, '/', $file-name)
             let $file-system-path := concat($export-dir, $file-separator, $file-name)
             let $file-doc := doc($db-file-path)/*
             (: file:serialize($node-set as node()*, $path as item(), $parameters as xs:string*, 
     $append as xs:boolean) as xs:boolean? :)
             let $store := file:serialize($file-doc, $file-system-path, '', false())
             return
                <tr>
                  <td>file:{$file-system-path}</td>
                  <td>{$store}</td>
                </tr>
        }
      </tbody>
   </table>
   </div>
   <br/>
   <div class="row col-md-12">
   Note: To verify the files are present you can copy the file path into the browser or file explorer.
   </div>
</div>

return style:assemble-page($title, $content)