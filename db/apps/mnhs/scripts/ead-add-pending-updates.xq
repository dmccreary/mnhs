xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';


(: Get a list of update command from a table-based form that has checkboxes in a update column 
   Incomming data must be in request:get-parameter-names() :)

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $title := 'EAD Add Pending Updates'

(: this line takes all the checkbox input parameters and appends a list of <update> elements to the pending-updates.xml file :)
let $update-list := ead:append-updates-from-input-parameters()

let $content :=
<div class="content">
   The following names have been added to the pending updates list.
   <form method="post" action="ead-run-updates.xq">
       <table class="table table-striped table-bordered table-hover table-condensed">
          <thead>
             <tr>
                <th>#</th>
                <th>File</th>
                <th>Element Name</th>
                <th>Old Name</th>
                <th>LOC ID</th>
                <th>LOC Name</th>
             </tr>
          </thead>
         {for $update at $count in $update-list
            (: <update>
                <input-string>{$input}</input-string>
                <file-path></file-path>
                <element-name></element-name>
                <old-name></old-name>
                <new-id></new-id>
                <new-name></new-name>
             </update>
            :)
            return
               <tr>
                 <th>{$count}</th>
                 
                 <td>{$update/file-path/text()}</td>
                 <td>{$update/element-name/text()}</td>
                 <td>{$update/old-name/text()}</td>
                 <td>{$update/new-id/text()}</td>
                 <td>{$update/new-name/text()}</td>
               </tr>
         }
       </table>
       <button type="submit">Run ALL EAD Updates</button>
   </form>
</div>

return if ($debug)
  then $update-list
  else style:assemble-page($title, $content)