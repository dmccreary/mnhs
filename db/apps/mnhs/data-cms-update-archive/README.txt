This collection contains archives of prior updates.
After each pending-update.xml file is done a file is placed in this collection that contains the updates.
The file will be of the format 'archive-yyyy-mm-ddTHH-MM.xml'