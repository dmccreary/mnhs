xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";
declare namespace atom="http://www.w3.org/2005/Atom";
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes indent=yes";

let $input := 'Mondale'
let $approximate-match := loc:approximate-match-html-table($input)
return

<html>
 {$approximate-match}
</html>