xquery version "1.0";

import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

(: copy our test file into /db/tmp :)
let $collection := '/db/apps/mnhs/data-ead-test'
let $eadid := '00000'
let $element-name := 'persname'
let $name := 'Norris, William C., 1911-2006.'

(:
let $input := doc('/db/apps/mnhs/data-ead-test/00000.xml')/ead

let $create-tmp-collection-if-needed := 
   if (xmldb:collection-available('/db/tmp'))
     then ()
     else xmldb:create-collection('/db','tmp')
     
let $store := xmldb:store ("/db/tmp", "ead-update-test.xml", $input)

(: get the first name :)
let $element := doc('/db/tmp/ead-update-test.xml')//corpname[1]

let $authfile-id := 'n12345'
:)

let $get := ead:get-name($collection, $eadid, $element-name, $name)

return
<testcase>
  <input>
    <collection>{$collection}</collection>
    <eadid>{$eadid}</eadid>
    <element-name>{$element-name}</element-name>
    <name>{$name}</name>
  </input>
  <result>{$get}</result>
</testcase>