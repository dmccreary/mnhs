xquery version "1.0";

import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

let $title := 'Remove Relationships'

let $test-cases :=
<tests>
   <name>McCreary, Dan</name>
   <name>McCreary, Dan, author</name>
   <name>McCreary, Dan, programmer</name>
   <name>Kelly, Ann</name>
   <name>Kelly, Ann, author</name>
   <name>Kelly, Ann, programmer</name>
   <name>Johnson, Paul, writer of supplementary textual content</name>
 </tests>

let $content :=
<div class="content col-md-7">
    <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>EAD Name</th>
              <th>Result</th>
           </tr>
        </thead>
        <tbody>{
          for $test at $count in $test-cases/name
          return
             <tr>
                 <td>{$test}</td>
                 <td>{ead:remote-relationship-designator($test)}</td>
             </tr> 
       }</tbody>
  </table>
</div>

return style:assemble-page($title, $content)