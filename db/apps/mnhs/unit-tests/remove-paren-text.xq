let $inputs :=
<inputs>
   <company-name>Minnesota Historical Society (St. Paul, MN)</company-name>
   <company-name>Young Matrons' Club (Minneapolis, Minn.).</company-name>
   <company-name>Fallon-McElligott (Minneapolis, Minn.).</company-name>
   <company-name>Midway Lumber Co. (Saint Paul, Minn.).</company-name>
   <company-name>Midway-Platt Company (Saint Paul, Minn.).</company-name>
   <company-name>Payless Cashways, Inc. (Kansas City, Mo.).</company-name>
   <company-name>Platt Hardware Co. (Saint Paul, Minn.).</company-name>
   <company-name>Shelter Corporation of America, Inc. (Minneapolis, Minn.).</company-name>
   <company-name>Southwest Forest Industries (Phoenix, Ariz.).</company-name>
   <company-name>Twentieth Century Club (Minneapolis, Minn.).</company-name>
   <company-name>Minnesota Landmarks (Agency).</company-name>
   <company-name>Ordway Music Theatre (Saint Paul, Minn.).</company-name>
   <company-name>Minnesota. Governor (1951-1955 : Anderson) -- Correspondence.</company-name>
   <company-name>Saint Paul (Minn.). Department of Police.</company-name>
   <company-name>National Youth Anti-War Congress (1939 : Chicago, Ill.).</company-name>
   <company-name>National Pacifist Youth Conference (1939 : Milford, Ind.).</company-name>
   <company-name>Democratic Party (U.S.).</company-name>
   <company-name>Human Rights Campaign Fund (Washington, D.C.).</company-name>
   <company-name>New American Movement (Organization). Minneapolis Chapter.</company-name>
   <company-name>Minneapolis (Minn.). City Council.</company-name>
   <company-name>Alive and Trucking Theater (Minneapolis, Minn.).</company-name>
   <company-name>American Indian Business Development Corporation (Minneapolis, Minn.).</company-name>
   <company-name>Coalition for Affordable Housing (Minneapolis, Minn.).</company-name>
   <company-name>East-West Bank Tenants Union (Minneapolis, Minn.).</company-name>
   <company-name>Farmer-Labor Association (Minn.).</company-name>
   <company-name>Group Health Plan, inc.</company-name>
</inputs>

return
<results>{
for $company-name in $inputs/company-name
   let $regex := replace($company-name, '\(.*\)', '')
   let $last-char := substring($regex, string-length($regex), 1)
   let $normalized-name :=
      if ($last-char = '.')
         then normalize-space(substring($regex, 1, string-length($regex) - 1))
         else normalize-space($regex)
   return
   <r i="{$company-name}"a="{$regex}" b="{$last-char}" c="{$normalized-name}">
     
   </r>
}</results>