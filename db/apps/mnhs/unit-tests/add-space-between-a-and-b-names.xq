xquery version "1.0";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";
(:
taken from http://id.loc.gov/authorities/names/no90027449.marcxml.xml
:)

let $marcxml-record :=
<marcxml:record xmlns:xlink="http://www.w3.org/1999/xlink" 
xmlns:marcxml="http://www.loc.gov/MARC21/slim" 
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" 
xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:mets="http://www.loc.gov/METS">
    <marcxml:datafield tag="110" ind1="1" ind2=" ">
        <marcxml:subfield code="a">Saint Paul (Minn.).</marcxml:subfield>
        <marcxml:subfield code="b">Department of Police</marcxml:subfield>
    </marcxml:datafield>
</marcxml:record>

(: here is our test where we put spaces between the a and b names :)
let $new-name :=
    string-join(
       $marcxml-record//marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield
      , ' ')

(: the result should be :)
let $expected-result := 'Saint Paul (Minn.). Department of Police'

let $pass-fail :=
if ($new-name = $expected-result)
  then 'pass'
  else 'fail'
return
<testcase name="join a and be names with a space" classname="marcxml name-builder">
   <input>{$marcxml-record}</input>
   <actual-result>{$new-name}</actual-result>
   <expected-result>{$expected-result}</expected-result>
   <pass-fail>{$pass-fail}</pass-fail>
</testcase>