xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $input := 'Minnesota Historical Society'
(:
let $uri := loc:create-search-uri($input)


let $literal-uri := xs:anyURI('http://id.loc.gov/search/?q=Minnesota%20Historical%20Society&format=atom')
let $real-run := httpclient:get($literal-uri, false(), <params/>):)

let $run := loc:run($input)
let $result := loc:match-exact($input)

return
<testcase>
  <input>{$input}</input>
  <run>{$run//atom:entry/atom:title}</run>
  <result>{$result}</result>
</testcase>