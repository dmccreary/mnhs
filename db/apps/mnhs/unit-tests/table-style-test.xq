xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';

let $title := 'Table Style Test'

let $content :=
<div class="content">
   Test of CSS style rules.
  
   <table class="table table-striped table-bordered table-hover table-condensed">
       <thead>
            <tr>
               <th colspan="3"  class="border-right">CMS Fields</th>
               <th colspan="3">Library of Congress Search Results</th>
            </tr>
            <tr>
               <th>#</th>
               <th>Name</th>
               <th class="border-right">Party Type</th>

               
               <!-- <td>loc-results</td> -->
               <th>Match Count</th>
               <th>Exact Match ID</th>
               <th>Preferred Name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
            <tr>
               <td>Col 1</td>
               <td>Col 2</td>
               <td class="border-right">Col 3</td>
               <td>Col 4</td>
               <td>Col 5</td>
               <td>Col 6</td>
            </tr>
      </tbody>
   </table>
    Sample Rules to Check:
    <ol>
      <li>Alternating rows are white and light gray</li>
      <li>Table headers are centered.</li>
      <li>Table headers and in bold.</li>
      <li>Table body values are left jusfified.</li>
      <li>Adding a class="border-right" should add a black line to the right of a table cell.</li>
   </ol>
</div>

return style:assemble-page($title, $content)