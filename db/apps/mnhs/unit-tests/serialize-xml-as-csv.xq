xquery version "1.0";
declare option exist:serialize "method=text media-type=text/csv omit-xml-declaration=yes";
 
let $nl := "&#10;"
 
let $input :=
<rows>
   <row>
      <c1>Row1 Col1</c1>
      <c2>Row1 Col2</c2>
      <c3>Row1 Col3</c3>
      <c4>Row1 Col4</c4>
   </row>
   <row>
      <c1>Row2 Col1</c1>
      <c2>Row2 Col2</c2>
      <c3>Row2 Col3</c3>
      <c4>Row2 Col4</c4>
   </row>
   <row>
      <c1>Row3 Col1</c1>
      <c2>Row3 Col2</c2>
      <c3>Row3 Col3</c3>
      <c4>Row3 Col4</c4>
   </row>
   <row>
      <c1>Row4 Col1</c1>
      <c2>Row4 Col2</c2>
      <c3>Row4 Col3</c3>
      <c4>Row2 Col4</c4>
   </row>
</rows>
 
let $csv-string :=
  string-join(
        for $row in $input//row
        return
          string-join(
             for $col in $row/*
             return
                $col/text()
          , ',')
    , $nl)

(: set the file name :)
let $set-file-name := response:set-header('Content-Disposition',  'attachment; filename="table.csv"')
(: 
http://exist-db.org/exist/apps/fundocs/view.html?uri=http://exist-db.org/xquery/response&location=java:org.exist.xquery.functions.response.ResponseModule
:)
return $csv-string
