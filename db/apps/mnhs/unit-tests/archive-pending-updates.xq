xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";

(: the current system is ead or cms :)
let $system := request:get-parameter('system', 'ead')

(: this one line does it all :)
let $archive-cmd := loc:archive-pending-updates($system)

(: if the file exists, we have a success :)
let $test-result :=
  if (doc($archive-cmd/archive-file-path)/pending-updates)
     then 'success'
     else 'failure'

let $current-data-collection := 
  if ($system = 'cms')
     then $config:cms-data
     else $config:ead-data

let $pending-file-path := concat($current-data-collection, '/', $config:pending-update-file-name)
let $content :=
<results>
  {$archive-cmd}
  <current-pending-updates-count>
    { count(doc($pending-file-path)//updates) }
  </current-pending-updates-count>
</results>

return
<testcase name="loc:archive-pending-updates" classname="loc">
   {element {$test-result} {$content/*} }
</testcase>