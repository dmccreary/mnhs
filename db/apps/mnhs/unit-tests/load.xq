xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace xf="http://www.w3.org/2002/xforms";
declare namespace ev="http://www.w3.org/2001/xml-events";

let $debug := xs:boolean(request:get-parameter('debug', 'true'))

let $title := 'Style Assemble Form Test'

let $style := <style/>

let $model :=
<xf:model>
    <xf:instance xmlns="" id="save-data">
        <data>
            <PersonGivenName/>
        </data>
    </xf:instance>
</xf:model>

let $content :=
<div class="content">
<xf:trigger>
   <xf:label>Load</xf:label>
   <xf:action ev:event="DOMActivate">
      <xf:load show="replace">
         <xf:resource value="'https://www.google.com/search?q=site%3Ahttps%3A%2F%2Fen.wikibooks.org%2Fwiki%2FXForms+load'"/>
      </xf:load>
   </xf:action>
</xf:trigger>

    {if ($debug)
       then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
       else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
    }
</div>

return style:assemble-form($title, $model, $style, $content, $debug)