xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

let $q := 'n79018723'
let $run := loc:create-search-uri-for-marcxml($q)

return
<testcase>
  <input>{$q}</input>
  <results>{$run}</results>
</testcase>