import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'Marc Subfield Test'

let $test-set-collection := concat($config:app-home-collection, '/unit-tests/marc-subfield-tests')
let $test-files := xmldb:get-child-resources($test-set-collection)

let $test-marc-files :=
<marc-test-files>
  {for $file in $test-files
   let $file-path := concat($test-set-collection, '/', $file) 
   return doc($file-path)
   }
</marc-test-files>

let $test-data-file-path := concat($config:app-home-collection, '/unit-tests/marc-subfield-tests.xml')
let $test-data := doc($test-data-file-path)
let $content :=
<div class="content">
  <style type="text/css">
      table tbody tr td, table tbody tr th{{font-size:8pt; }}
  </style>
  <table class="table table-narrow">
    <thead>
      <tr>
         <th>#</th>
         <th>ID</th>
         <th>Expected</th>
         <th>Actual</th>
         <th>Pattern</th>
         <th>Status</th>
       </tr>
     </thead>
     <tbody>
        {for $marcrecord at $count in $test-marc-files/marcxml:record
         let $locid := $marcrecord//marcxml:controlfield[@tag='001']/text()
         let $test := $test-data//test[locid=$locid]
         let $expected-name := $test/name/text()
         let $pattern := $test/pattern/text()
         let $calculated-name := loc:name-from-subfields($marcrecord)
         return
           <tr>
             <th>{$count}</th>
             <td><a href="./marc-subfield-tests/{$locid}.marcxml.xml" target="_blank">{$locid}</a></td>
             <td>{$expected-name}</td>
             <td>{$calculated-name}</td>
             <td>{$pattern}</td>
             <td>{if ($expected-name = $calculated-name)
                then <span class="text-success">pass</span>
                else <span class="text-danger">fail</span>
                }</td>
           </tr>
        }
     </tbody>
   </table>
</div>

return style:assemble-page($title, $content)