xquery version "1.0";

import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

let $test-file := doc('/db/apps/mnhs/data-ead-test/00000.xml')
let $store := xmldb:store('/db/tmp', '00000.xml', $test-file)

let $run-updates :=
    for $name in doc('/db/tmp/00000.xml')//(persname|corpname)
    return
      if ($name/@authfilenumber)
         then ()
         else ead:update-name-element($name, 'new-id', 'new-name')
 
 return
 <results>
    <before>
       {$test-file}
    </before>
    <after>
       {doc('/db/tmp/00000.xml')}
    </after>
 </results>