xquery version "1.0";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

let $count := $ead:count-names

return
<results>
  <name-count>{$count}</name-count>
  <corp-count>{count($ead:corp-names)}</corp-count>
  <pers-count>{count($ead:pers-names)}</pers-count>
  <fam-count>{count($ead:fam-names)}</fam-count>
  <names-with-sources>{$ead:names-with-sources}</names-with-sources>
  <names-with-authfilenumber>{$ead:names-with-authfilenumber}</names-with-authfilenumber>
  <file-names-with-famname>
    { for $fam-name in $ead:fam-names
    return
      util:document-name($fam-name)
    }
  </file-names-with-famname>
</results>