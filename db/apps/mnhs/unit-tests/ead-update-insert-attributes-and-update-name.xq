let $test-file := '/db/apps/mnhs/data-ead/00003-2.xml'

(:
            <controlaccess>
                <head>Persons:</head>
                <persname encodinganalog="700">Ades, C. Douglas. </persname>
                <persname encodinganalog="700">Bemis, Judson, 1913-2001. </persname>
                <persname encodinganalog="700" source="lcnaf" authfilenumber="no99005797">Boschee, Jerr</persname>
                <persname encodinganalog="700">Norris, William C., 1911-2006</persname>
                <persname encodinganalog="700" source="lcnaf" authfilenumber="nr95023315">Price, Robert M., 1930-</persname>
            </controlaccess>
:)

let $name := doc($test-file)//persname[.='Norris, William C., 1911-2006']

(:
    <update>
        <input-string>/db/apps/mnhs/data-ead/00003-2.xml|persname|Norris, William C., 1911-2006|n82259307|Norris, William C., 1911-2006|</input-string>
        <file-path>/db/apps/mnhs/data-ead/00003-2.xml</file-path>
        <element-name>persname</element-name>
        <old-name>Norris, William C., 1911-2006</old-name>
        <new-id>n82259307</new-id>
        <new-name>Norris, William C., 1911-2006x</new-name>
    </update>
 :)
 
 (: the new authfilenumber is n82259307 :)
 let $update1 := update insert attribute authfilenumber {'n82259307'} into $name
 let $update1 := update insert attribute source {'lcnaf'} into $name
 let $update-name := update value $name with 'Norris, William C., 1911-2006'
 
 return
 <result>
   {$name}
 </result>