xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

let $input := 'Minnesota Historical Society'
(:
let $uri := loc:create-search-uri($input)


let $literal-uri := xs:anyURI('http://id.loc.gov/search/?q=Minnesota%20Historical%20Society&format=atom')
let $real-run := httpclient:get($literal-uri, false(), <params/>):)

let $run := loc:run($input)

return
<testcase>
  <input>{$input}</input>
  <results>{$run}</results>
</testcase>