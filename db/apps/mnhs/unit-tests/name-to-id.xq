xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $name := request:get-parameter('name', 'Minnesota Historical Society')

let $exact-title := loc:match-exact($name)
let $id := loc:id($name)

return
<testcase>
  <name>{$name}</name>
  <exact-title>{$exact-title}</exact-title>
  <id>{$id}</id>
</testcase>