xquery version "1.0";

let $items := doc('../code-tables/relationship-designators.xml')//item

return
<items>
  {for $item in $items
   order by $item/text()
   return $item
   }
</items>