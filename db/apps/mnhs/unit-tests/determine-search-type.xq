xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $input := 
<table name="eparties">
    <tuple>
        <table name="CmsParty">
            <tuple>
                <atom name="irn" type="text" size="short">10072292</atom>
                <atom name="NamPartyType" type="text" size="short">Organization</atom>
                <atom name="SummaryData" type="text" size="short">A.C. Gilbert Company</atom>
                <atom name="BioBirthEarliestDate" type="text" size="short"/>
                <atom name="BioDeathEarliestDate" type="text" size="short"/>
                <atom name="AutAuthority" type="text" size="short">Library of Congress Name Authority File</atom>
                <atom name="AutAuthorityID" type="text" size="short">n79018723</atom>
                <atom name="AutDisplayName" type="text" size="short"/>
            </tuple>
        </table>
    </tuple>
</table>
let $run := loc:determine-search-type($input)

return
<testcase>
  <input>{$input}</input>
  <results>{$run}</results>
</testcase>