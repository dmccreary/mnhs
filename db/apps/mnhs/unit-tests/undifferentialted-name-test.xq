xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";
declare namespace atom="http://www.w3.org/2005/Atom";


let $sample-undifferentiated-names := ('Doe, John', 'Smith, Jane')

let $loc-search-results :=
   for $input in $sample-undifferentiated-names
   return
     loc:approximate-match-html-table($input)
return

<html>
 {$loc-search-results}
</html>