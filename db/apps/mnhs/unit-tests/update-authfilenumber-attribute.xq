xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $input := 
<root>
<foo-bar authfilenumber = "foobar"></foo-bar>
</root>

let $create-tmp-collection-if-needed := 
   if (xmldb:collection-available('/db/tmp'))
     then ()
     else xmldb:create-collection('/db','tmp')

let $store := xmldb:store ("/db/tmp", "test.xml", $input)

let $element := doc('/db/tmp/test.xml')//foo-bar
let $authfile-id := 'n12345'
let $update := loc:insert-authfilenumber-attribute($element, $authfile-id)

return
<testcase>
  <input>{$input}</input>
  <result>{$update}</result>
</testcase>