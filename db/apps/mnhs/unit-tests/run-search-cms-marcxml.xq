xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

let $input := 'n2001081177'

let $run := loc:run-marcxml($input)

return
<testcase>
  <input>{$input}</input>
  <results>{$run}</results>
</testcase>