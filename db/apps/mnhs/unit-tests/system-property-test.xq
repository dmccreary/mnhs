xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

let $title := 'Export EAD Data'
let $exist-home := environment-variable('JAVE_HOME')

   (:
   "file.separator"	Character that separates components of a file path. This is "/" on UNIX and "\" on Windows.
"java.class.path"	Path used to find directories and JAR archives containing class files. Elements of the class path are separated by a platform-specific character specified in the path.separator property.
"java.home"	Installation directory for Java Runtime Environment (JRE)
"java.vendor"	JRE vendor name
"java.vendor.url"	JRE vendor URL
"java.version"	JRE version number
"line.separator"	Sequence used by operating system to separate lines in text files
"os.arch"	Operating system architecture
"os.name"	Operating system name
"os.version"	Operating system version
"path.separator"	Path separator character used in java.class.path
"user.dir"	User working directory
"user.home"	User home directory
"user.name"	User account name
:)

let $content :=
<div class="content">
   <h4>System Properties</h4>
   <div class="row col-md-7">
   <table class="table table-striped table-bordered table-hover table-condensed ">
        <thead>
           <tr>
              <th>Property</th>
              <th>Value</th>
           </tr>
        </thead>
        <tbody>
        {for $property in ('vendor', 'vendor-url', 'product-name', 'product-version', 'product-build', 'file.separator')
           return
               <tr>
                <td>{$property}</td>
                <td>{util:system-property($property)}</td>
              </tr>
        }
        </tbody>
   </table>
   
   <h4>Java Properties</h4>

   <table class="table table-striped table-bordered table-hover table-condensed ">
        <thead>
           <tr>
              <th>Property</th>
              <th>Value</th>
           </tr>
        </thead>
        <tbody>
        {for $property in ('file.separator', 'java.home', 'java.vendor', 'java.vendor.url', 'java.version',
        'line.separator', 'os.arch', 'os.name', 'os.version', 'path.separator', 'user.dir', 'user.home',
        'user.name')
           return
               <tr>
                <td>{$property}</td>
                <td>{util:system-property($property)}</td>
              </tr>
        }
        </tbody>
   </table>

   
   <h4>Environment Variables</h4>
   <table class="table table-striped table-bordered table-hover table-condensed ">
        <thead>
           <tr>
              <th>Variable</th>
              <th>Value</th>
           </tr>
        </thead>
        <tbody>
        {for $variable in available-environment-variables()
           return
               <tr>
                <td>{$variable}</td>
                <td>{environment-variable($variable)}</td>
              </tr>
        }
        </tbody>
   </table>
   </div>
   <br/>
   <div class="row col-md-12">
   Note: To verify the files are present you can copy the file path into the browser or file explorer.
   </div>
</div>

return style:assemble-page($title, $content)