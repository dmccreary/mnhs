xquery version "1.0";
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $q := request:get-parameter('q', 'Minnesota Landmarks (Agency)')

let $file-name := '00009-1.xml'
let $app-collection := $style:app-home
let $file-path := concat($app-collection, '/data/', $file-name)

let $input := doc($file-path)/ead

let $input-element := $input//corpname[normalize-space(replace(./text(), '\.', '')) = $q]

let $create-tmp-collection-if-needed := 
   if (xmldb:collection-available('/db/tmp'))
     then ()
     else xmldb:create-collection('/db','tmp')

let $store := xmldb:store ("/db/tmp", $file-name, $input)

let $new-doc := doc(concat('/db/tmp/', $file-name))/ead

let $element := $new-doc//corpname[normalize-space(replace(./text(), '\.', '')) = $q]

(:
<corpname role="subject" encodinganalog="610">Minnesota Landmarks
			 (Agency). </corpname>
			 :)
			 
let $loc-search := loc:match-exact($q)

let $loc-id-url := $loc-search/atom:id/text()

let $loc-id := loc:url-to-id($loc-id-url)

let $update := loc:insert-authfilenumber-attribute($element, $loc-id)

let $output-element := $new-doc//corpname[normalize-space(replace(./text(), '\.', '')) = $q]

return
<testcase>
  <q>{$q}</q>
  <corpname-count>{count($input//corpname)}</corpname-count>
  <input-element>{$input-element}</input-element>
  <loc-search>{$loc-search}</loc-search>
  <loc-id>{$loc-id}</loc-id>
  <output-element>{$output-element}</output-element>
</testcase>