xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'Undifferentiated Name Test'

(:
Doe, John
n87930115
http://id.loc.gov/authorities/names/n87930115.html

Smith, Jane
n93803718
http://id.loc.gov/authorities/names/n93803718.html
:)

(: Walter Mondale and Jessie Ventura :)
let $normal-ids := ('n79077424', 'n85247381')
let $sample-undifferentiated-ids := ('n87930115', 'n93803718')
let $all := ($normal-ids, $sample-undifferentiated-ids)

let $content :=
<div class="content">
  Sample of Differentiated and Undifferentiated Records
  <table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
            <tr>
               <th>LOC ID</th>
               <th>Name</th>
               <th>Undifferentiated Name</th>
               <th>Field 8</th>
               <th>Column 33</th>
               <th>Function</th>
            </tr>
    </thead>
    <tbody>
    {
  for $id in $all
  let $marcrecord := loc:get-one-marcxml-with-id($id)
  return
    <tr>
       <td>{$id}</td>
       <td>{loc:get-preferred-name-from-marc($marcrecord)}</td>
       <td>{loc:undifferentiated-name($marcrecord)}</td>
       <td><pre>{$marcrecord/marcxml:controlfield[@tag = '008']}</pre></td>
       <td>{substring($marcrecord/marcxml:controlfield[@tag = '008'], 33, 1)}</td>
       <td>{loc:undifferentiated-name($marcrecord)}</td>
    </tr>
    }
     </tbody>
   </table>
</div>

return style:assemble-page($title, $content)