xquery version "1.0";

import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

(:
<tuple>
    <atom name="irn" type="text" size="short">10127119</atom>
    <atom name="NamPartyType" type="text" size="short">Organization</atom>
    <atom name="SummaryData" type="text" size="short">A. A. Richardson Photo-Illustrating Company</atom>
    <atom name="BioBirthEarliestDate" type="text" size="short"/>
    <atom name="BioDeathEarliestDate" type="text" size="short"/>
    <atom name="AutAuthority" type="text" size="short">Library of Congress Name Authority File</atom>
    <atom name="AutAuthorityID" type="text" size="short"/>
    <atom name="AutDisplayName" type="text" size="short"/>
</tuple>
:)

let $irn := '10127119'
let $old-name := 'A. A. Richardson Photo-Illustrating Company'
let $new-name := 'foobar'

return
<results>
  <input>
   <irn>{$irn}</irn>
  </input>
  <result>
    {cms:update-tuple($irn, $new-name)}
    {cms:get-tuple($irn)}
   </result>
</results>