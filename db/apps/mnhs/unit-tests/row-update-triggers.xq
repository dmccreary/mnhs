xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace xf="http://www.w3.org/2002/xforms";
declare namespace ev="http://www.w3.org/2001/xml-events";

let $debug := xs:boolean(request:get-parameter('debug', 'true'))

let $title := 'Row Update Triggers'

let $style := <style/>

let $model :=
<xf:model>
    <xf:instance xmlns="" id="save-data">
        <data>
            <row>
               <name>Walter Mondale</name>
            </row>
            <row>
               <name>Joan Mondale</name>
            </row>
            <row>
               <name>Jesse Ventura</name>
            </row>
        </data>
    </xf:instance>
    
    <xf:instance id="get-results" xmlns="">
      <data/>
    </xf:instance>
    
    <xf:instance id="log-instance" xmlns="">
        <data>
            <event>initital event</event>
        </data>
    </xf:instance> 
    
    <xf:submission id="update-name-id" method="get" action="../echo-get.xq" 
      ref="save-data" replace="instance" instance="get-results">
        <xf:action ev:event="xforms-submit">
            <xf:insert nodeset="instance('log-instance')/event" at="last()" position="after"/>
            <xf:setvalue ref="instance('log-instance')/event[last()]" 
                        value="xforms-submit"/>
         </xf:action>
    </xf:submission>
</xf:model>

let $content :=
<div class="content">
    <xf:repeat ref="instance('save-data')//row" id="row-repeat">
       <xf:output value="position()"/>
        <xf:output ref="name">
            <xf:label>Name:</xf:label>
        </xf:output>
        
        <br/>
        
        <xf:trigger>
           <xf:label>update</xf:label>
           <xf:action ev:event="DOMactivte">
              <xf:send submission="update-name-id">
              </xf:send>
           </xf:action>
        </xf:trigger>
    </xf:repeat>
    <br/>
    
    <div id="log-div">
        <h4>Event Log</h4>
        <xf:repeat id="results-repeat" ref="instance('log-instance')/event">
            <xf:output ref="."/>
        </xf:repeat>
    </div>
    {if ($debug)
       then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
       else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
    }
</div>

return style:assemble-form($title, $model, $style, $content, $debug)