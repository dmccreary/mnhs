xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $input1 := doc("sample-marcxml.xml")/marcxml:record

let $run1 := loc:marcxml-preferred-name($input1)

return
<testcases>
  <testcase name="extract-preferred-name" classname="loc:marcxml-preferred-name">
    <input>{$input1/marcxml:datafield[@tag = '110']}</input>
    <results1>{$run1}</results1>
  </testcase>
</testcases>
