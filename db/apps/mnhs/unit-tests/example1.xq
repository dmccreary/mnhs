xquery version "1.0";

let $encode := encode-for-uri('Minnesota Historical Society')

let $concat := concat('http://id.loc.gov/search/?q=', $encode, '&amp;format=atom')

let $search-url := xs:anyURI($concat)

let $get := httpclient:get($search-url, false(), <params/>)

return
<results>
  {$get}
</results>