xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'Undifferentiated Name Test'

(: test of this path substring($marcxml/marcxml:controlfield[@tag = '008'], 33, 1) = 'b' :)

let $marc-record-1 :=
<marcxml:record xmlns:marcxml="http://www.loc.gov/MARC21/slim" xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:mets="http://www.loc.gov/METS/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <marcxml:leader>00567nz  a2200169n  4500</marcxml:leader>
    <marcxml:controlfield tag="001">n2001081177</marcxml:controlfield>
    <marcxml:controlfield tag="003">DLC</marcxml:controlfield>
    <marcxml:controlfield tag="005">20011002083329.0</marcxml:controlfield>
    <marcxml:controlfield tag="008">011002n| acannaabn          |a ana      </marcxml:controlfield>
    <marcxml:datafield tag="010" ind1=" " ind2=" ">
        <marcxml:subfield code="a">n 2001081177</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="040" ind1=" " ind2=" ">
        <marcxml:subfield code="a">DLC</marcxml:subfield>
        <marcxml:subfield code="b">eng</marcxml:subfield>
        <marcxml:subfield code="c">DLC</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="110" ind1="2" ind2=" ">
        <marcxml:subfield code="a">A.J. Reach Co.</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="410" ind1="2" ind2=" ">
        <marcxml:subfield code="a">Reach Co.</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="410" ind1="2" ind2=" ">
        <marcxml:subfield code="w">nnnn</marcxml:subfield>
        <marcxml:subfield code="a">Reach, A.J. Co., Philadelphia</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="670" ind1=" " ind2=" ">
        <marcxml:subfield code="a">NUCMC data from Garden Peninsula Historical Society Museum for Reach's official score book, [19--]</marcxml:subfield>
        <marcxml:subfield code="b">(A.J. Reach Co.; Philadelphia, Pa.)</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="670" ind1=" " ind2=" ">
        <marcxml:subfield code="a">LC data base, Oct. 02 2001</marcxml:subfield>
        <marcxml:subfield code="b">(hdg.: Reach, A.J. Co., Philadelphia)</marcxml:subfield>
    </marcxml:datafield>
</marcxml:record>

let $marc-record-2 :=
<marcxml:record xmlns:marcxml="http://www.loc.gov/MARC21/slim" xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:mets="http://www.loc.gov/METS/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <marcxml:leader>00567nz  a2200169n  4500</marcxml:leader>
    <marcxml:controlfield tag="001">n2001081177</marcxml:controlfield>
    <marcxml:controlfield tag="003">DLC</marcxml:controlfield>
    <marcxml:controlfield tag="005">20011002083329.0</marcxml:controlfield>
    <!-- change this one from "n" to "b" at character 33 -->
    <marcxml:controlfield tag="008">011002n| acannaabn          |a aba      </marcxml:controlfield>
    <marcxml:datafield tag="010" ind1=" " ind2=" ">
        <marcxml:subfield code="a">n 2001081177</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="040" ind1=" " ind2=" ">
        <marcxml:subfield code="a">DLC</marcxml:subfield>
        <marcxml:subfield code="b">eng</marcxml:subfield>
        <marcxml:subfield code="c">DLC</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="110" ind1="2" ind2=" ">
        <marcxml:subfield code="a">A.J. Reach Co.</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="410" ind1="2" ind2=" ">
        <marcxml:subfield code="a">Reach Co.</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="410" ind1="2" ind2=" ">
        <marcxml:subfield code="w">nnnn</marcxml:subfield>
        <marcxml:subfield code="a">Reach, A.J. Co., Philadelphia</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="670" ind1=" " ind2=" ">
        <marcxml:subfield code="a">NUCMC data from Garden Peninsula Historical Society Museum for Reach's official score book, [19--]</marcxml:subfield>
        <marcxml:subfield code="b">(A.J. Reach Co.; Philadelphia, Pa.)</marcxml:subfield>
    </marcxml:datafield>
    <marcxml:datafield tag="670" ind1=" " ind2=" ">
        <marcxml:subfield code="a">LC data base, Oct. 02 2001</marcxml:subfield>
        <marcxml:subfield code="b">(hdg.: Reach, A.J. Co., Philadelphia)</marcxml:subfield>
    </marcxml:datafield>
</marcxml:record>

let $test-1 := loc:undifferentiated-name($marc-record-1)
let $char-33-1 := substring($marc-record-1/marcxml:controlfield[@tag = '008'], 33, 1)

let $test-2 := loc:undifferentiated-name($marc-record-2)
let $char-33-2 := substring($marc-record-2/marcxml:controlfield[@tag = '008'], 33, 1)

let $pass-fail :=
  if (not($test-1) and $test-2)
     then 'success'
     else 'failure'

let $content :=
   <test-results xmlns:marcxml="http://www.loc.gov/MARC21/slim">
      <test-1>{$test-1}</test-1>
      <control-field-8>{$marc-record-1/marcxml:controlfield[@tag='008']}</control-field-8>
      <char-33-1>{$char-33-1}</char-33-1>
      <test-2>{$test-2}</test-2>
      <control-field-8>{$marc-record-2/marcxml:controlfield[@tag='008']}</control-field-8>
      <char-33-2>{$char-33-2}</char-33-2>
   </test-results>

return
  <testcase name="loc:undifferentiated-name" classname="http://mnhs.org/loc-search">
    {element {$pass-fail} {$content}}
  </testcase>