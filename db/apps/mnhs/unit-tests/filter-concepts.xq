xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'Concept filter on marc filter test'

let $test-collection := concat($config:app-home-collection, '/unit-tests/test-cases')

let $file := request:get-parameter('file', 'carleton-college.xml')

let $other-files := xmldb:get-child-resources($test-collection)

let $input-file-path := concat($test-collection, '/', $file)

return
  if (not(doc-available($input-file-path)))
     then <error><message>Document {$input-file-path} is not available</message></error>
     else (: continue :)
     
let $unfiltered-marcxml-records := doc($input-file-path)//marcxml:record

let $filtered-marcxml := loc:concept-match-filter($unfiltered-marcxml-records)
let $pre-filter-count := count($unfiltered-marcxml-records)
let $post-filter-count := count($filtered-marcxml)

let $sorted-marcrecords :=
  for $record in $unfiltered-marcxml-records
      let $primary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code='a']/text(), '-')
      let $secondary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code='b' or @code='t']/text(), '-')
      let $full-title := concat($primary-names, $secondary-names)
      order by $full-title
      return $record
      
let $content :=
<div class="content">
      
       Pre Concept Filter Count = {$pre-filter-count} Post Concept Filter Count = {$post-filter-count}<br/>

   <table class="table table-striped table-bordered table-hover table-condensed">
       <thead>
       <tr>
          <th>#</th>
          <th>Primary Titles</th>
          <th>Secondary Titles</th>
          <th>prim</th>
          <th>sec</th>
          <th>TKV</th>
          <th>Filter?</th>
          <th>Name</th>
       </tr>
     </thead>
     <tbody>{
       for $record at $count in $sorted-marcrecords
          let $primary := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="a"]/text()
          let $secondary := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="b"]/text()
          let $id := $record/marcxml:controlfield[@tag='001']/text()
       return
          <tr>
              <th>{$count}</th>
              <td>{$primary}</td>
              <td class="border-right">{$secondary}</td>
              <td>
                {if ($record/marcxml:datafield[@tag='100']/marcxml:subfield[@code="a"]/text())
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='110']/marcxml:subfield[@code="a"]/text()) 
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='111']/marcxml:subfield[@code="a"]/text())   
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='130']/marcxml:subfield[@code="a"]/text())    
                   then 'X'
                   else '_'
                }
                </td>
                
                <td>
                {if ($record/marcxml:datafield[@tag='100']/marcxml:subfield[@code="b"]/text())
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='110']/marcxml:subfield[@code="b"]/text()) 
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='111']/marcxml:subfield[@code="b"]/text())   
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='130']/marcxml:subfield[@code="b"]/text())    
                   then 'X'
                   else '_'
                }
                
              </td>
              
              <td class="border-right">
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 't')
                 then 't'
                 else '_'
              }
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 'k')
                 then 'k'
                 else '_'
              }
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 'v')
                 then 'v'
                 else '_'
              }

         </td>
              <td>
              {if (count(loc:concept-match-filter($record)) = 1)
                     then <span class="text-green">no</span>
                     else <span class="text-red">yes</span>
                     }
              </td>
              <td>
                 {if (string-length($id) gt 1)
                    then <a href="{loc:name-autority-id-to-uri($id)}" target="_blank">{$id}</a>
                    else ()
                 }
              </td>
          </tr> 
    }</tbody>
   </table>
   
   {for $file in $other-files
    return
       (<a href="{request:get-uri()}?file={$file}">{$file}</a>, <br/>)
   }
</div>

return style:assemble-page($title, $content)