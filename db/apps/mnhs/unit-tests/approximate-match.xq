xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $name := request:get-parameter('name', 'Minnesota Historical Society')

let $approximate-match := loc:approximate-match($name)

return
<testcase>
  <name>{$name}</name>
  <approximate-match>{$approximate-match}</approximate-match>
</testcase>