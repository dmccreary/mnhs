xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";
declare namespace atom="http://www.w3.org/2005/Atom";

let $input := 'A.C. Gilbert Company'

let $search-for-marcxml := loc:search-for-marcxml-ids($input)
(:  let $run := loc:run($input)
  let $entry := $run//atom:entry
  let $full-id := $entry/atom:id
  let $id := loc:url-to-id($full-id)
  let $retrieve-marcxml:= loc:run-marcxml($id)
  
    (:<results>{$run}</results>
  <entry>{$entry}</entry>
  <id>{$id}</id>:)
  <marcxml>{$retrieve-marcxml}</marcxml>:)
return
<testcase>
  <input>{$input}</input>
  <marcxml>{$search-for-marcxml}</marcxml>
  <search-summaryData>{loc:preferred-name-match-indicator($input, $search-for-marcxml)}</search-summaryData>
<intermediate-result>Datafields 100, 110, 111{$search-for-marcxml/marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield}</intermediate-result>
<subfield-result>Subfield result{$search-for-marcxml/marcxml:datafield/marcxml:subfield}</subfield-result>
<search-for-non-preferred>{loc:non-preferred-name-match-indicator($input, $search-for-marcxml)}</search-for-non-preferred>
</testcase>