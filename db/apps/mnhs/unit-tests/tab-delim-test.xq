xquery version "1.0";

import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";

let $file-name := 'tab-delim-test.txt'

let $table :=
<table>
   <thead>
    <tr>
       <th>Co1umn 1 Header</th>
       <th>Co1umn 2 Header</th>
       <th>Co1umn 3 Header</th>
    </tr>
   </thead>
   <tbody>
     <tr>
       <td>data col 1 row 1</td>
       <td>data col 2 row 1</td>
       <td>data col 3 row 1</td>
     </tr>
     <tr>
       <td>data col 1 row 2</td>
       <td>data col 2 row 2</td>
       <td>data col 3 row 2</td>
     </tr>
     <tr>
       <td>data col 1, row 3</td>
       <td>data col 2, row 3</td>
       <td>data col 3, row 3</td>
     </tr>
   </tbody>
</table>

return util2:table-to-tab-delimited($table, $file-name)