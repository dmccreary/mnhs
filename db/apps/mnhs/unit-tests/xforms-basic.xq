xquery version "1.0";

import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace xf="http://www.w3.org/2002/xforms";

let $title := 'Test of page assembler'
let $dummy-attributes := ()
let $debug := true()

let $style := ()

let $model :=
<xf:model>
    <xf:instance id="save-data">
        <data xmlns="">
            <PersonGivenName/>
        </data>
    </xf:instance>
</xf:model>

let $content :=
<div class="content">
    <h1>{$title}</h1>
    <xf:input ref="PersonGivenName" incremental="true">
        <xf:label>Please enter your first name: </xf:label>
    </xf:input>
    <br/>
    <xf:output ref="PersonGivenName">
        <xf:label>Hello: </xf:label>
    </xf:output>
</div>

(: style:assemble-form($model, $content, true()) :)
(: style:assemble-form($titleg, $dummy-attributes, $style, $model, $content, $debug) :)
return style:assemble-form($title, $dummy-attributes, $style, $model, $content, $debug)

