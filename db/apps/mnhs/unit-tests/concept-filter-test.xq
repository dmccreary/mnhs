xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'Concept Filter Titles'

let $input-collection := concat($config:app-home-collection, '/unit-tests/test-cases')

return
  if (not(xmldb:collection-available($input-collection)))
     then <error><message>Document {$input-collection} is not available</message></error>
     else (: continue :)
     
let $unfiltered-marcxml-records := collection($input-collection)//marcxml:record

let $filtered-marcxml := loc:concept-match-filter($unfiltered-marcxml-records)
let $pre-filter-count := count($unfiltered-marcxml-records)
let $post-filter-count := count($filtered-marcxml)

let $sorted-marcrecords :=
  for $record in $unfiltered-marcxml-records
      let $primary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code='a']/text(), '-')
      let $secondary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code='b' or @code='t']/text(), '-')
      let $full-title := concat($primary-names, $secondary-names)
      order by $full-title
      return $record
      
let $content :=
<div class="content">
       Test Cases Count := {count($unfiltered-marcxml-records)}
       Pre Concept Filter Count = {$pre-filter-count} Post Concept Filter Count = {$post-filter-count}<br/>
   Key Primary when subfield/@code='a',  Secondary when subfield/@code='b'
   XXXX represents @tab=100, 110, 111 or 130, Title is true when ANY @tag=130
   <table class="table table-striped table-bordered table-hover table-condensed">
       <thead>
       <tr>
          <th>#</th>
          <th>Primary Titles</th>
          <th>Secondary Titles</th>
          <th>prim</th>
          <th>sec</th>
          <th>TKV</th>
          <th>Title</th>
          <th>Filter?</th>
          <th>Name</th>
       </tr>
     </thead>
     <tbody>{
       for $record at $count in $sorted-marcrecords
          let $primary := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="a"]/text()
          let $secondary := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="b"]/text()
          let $title-record := if ($record/marcxml:datafield[@tag='130']) then true() else false()
          let $id := $record/marcxml:controlfield[@tag='001']/text()
       return
          <tr>
              <td>{$count}</td>
              <td>{$primary}</td>
              <td>{$secondary}</td>
              <td>
                {if ($record/marcxml:datafield[@tag='100']/marcxml:subfield[@code="a"]/text())
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='110']/marcxml:subfield[@code="a"]/text()) 
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='111']/marcxml:subfield[@code="a"]/text())   
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='130']/marcxml:subfield[@code="a"]/text())    
                   then 'X'
                   else '_'
                }
                </td>
                <td>
                {if ($record/marcxml:datafield[@tag='100']/marcxml:subfield[@code="b"]/text())
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='110']/marcxml:subfield[@code="b"]/text()) 
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='111']/marcxml:subfield[@code="b"]/text())   
                   then 'X'
                   else '_'
                }
                {if ($record/marcxml:datafield[@tag='130']/marcxml:subfield[@code="b"]/text())    
                   then 'X'
                   else '_'
                }
                
              </td>
              <td>
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 't')
                 then 't'
                 else '_'
              }
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 'k')
                 then 'k'
                 else '_'
              }
              { if  ($record/marcxml:datafield/marcxml:subfield/@code = 'v')
                 then 'v'
                 else '_'
              }

         </td>
         
         <td>
            { (: title :)
               if ($title-record)
                 then <span class="text-red">true</span>
                 else ()
            }
         </td>
         
              <td>{if (count(loc:concept-match-filter($record)) = 1)
                     then <span class="text-green">no</span>
                     else <span class="text-red">yes</span>
                     }
              </td>
              <td>{if (string-length($id) gt 1)
                    then <a href="{loc:name-autority-id-to-uri($id)}" target="_blank">{$id}</a>
                    else ()
                 }
              </td>
          </tr> 
    }</tbody>
   </table>
</div>

return style:assemble-page($title, $content)