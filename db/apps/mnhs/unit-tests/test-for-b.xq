xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $input1 := doc("sample-marcxml.xml")/marcxml:record
let $input2 := doc("sample-marcxml-2.xml")/marcxml:record

let $run1 := loc:is-008-position-32-b($input1)
let $run2 := loc:is-008-position-32-b($input2)
return
<testcases>
  <testcase name="positive-test-case" classname="loc:is-008-position-32-b">
    <input>{$input1/marcxml:controlfield[@tag = '008']}</input>
    <results1>{$run1}</results1>
  </testcase>
  <testcase name="negative-test-case" classname="loc:is-008-position-32-b">
      <input>{$input2/marcxml:controlfield[@tag = '008']}</input>
    <results2>{$run2}</results2>
  </testcase>
</testcases>
