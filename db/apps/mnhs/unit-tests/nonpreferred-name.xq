xquery version "1.0";

import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $input1 := doc("sample-marcxml.xml")/marcxml:record

let $run1 := loc:marcxml-non-preferred-name($input1)

return
<testcases>
  <testcase name="extract-non-preferred-name" classname="loc:marcxml-non-preferred-name">
    <input>{$input1/marcxml:datafield[@tag = '410']/marcxml:subfield[@code='a']}</input>
    <results1>{$run1}</results1>
  </testcase>
</testcases>

