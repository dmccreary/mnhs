xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
let $title := 'Minnesota Historical Society Search Demo Apps'

let $content :=
<div class="content">
     <p>This application performs search using the Libreary of Congress Name Lookup Service.</p>
     
     <h4>Main Unit Tests</h4>
     <a href="example1.xq">RAW RSS Atom Format</a> Raw RSS ATOM results for query "mondale"<br/>
     <a href="search-results-to-table.xq">Unstyled HTML Table</a> ATOM transformed to HTML table<br/>

     <a href="search-results-to-table-v2.xq">HTML Table v2</a> HTML Table with Bootstrap 3 CSS<br/>
     <a href="search-results-to-table-v3.xq">HTML Table with Bootstrap 3 CSS</a> HTML Table with Bootstrap 3 CSS<br/>
     <a href="search-results-to-table-v4.xq">HTML Table with Bootstrap 3 CSS and options</a> HTML Table with Bootstrap 3 CSS<br/>
     <a href="search-results-with-update.xq">HTML Table with with Update</a> HTML Table with Bootstrap 3 CSS<br/>

     
     <p>Please contact <a href="mailto:annmkelly1@gmail">Ann Kelly</a> if you have any feedback on this app.</p>
</div>

return style:assemble-page($title, $content)