xquery version "1.0";

let $search-url := xs:anyURI('http://id.loc.gov/search/?q=mondale&amp;format=atom')

let $get := httpclient:get($search-url, false(), <params/>)

return
<results>
  {$get}
</results>