
xquery version "1.0";

let $q := request:get-parameter('q', '')
let $front := 'http://id.loc.gov/search/?q='
let $back := '&amp;q=cs%3Ahttp%3A%2F%2Fid.loc.gov%2Fauthorities%2Fnames&amp;format=atom'

let $search-url := xs:anyURI( concat($front, $q, $back) )

let $get := httpclient:get($search-url, false(), <params/>)

return
<results>
  <input>
    <q>{$q}</q>
    <url>{$search-url}</url>
 </input>
 <output>
    {$get}
 </output>
  
</results>
