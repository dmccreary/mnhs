xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace httpclient = "http://exist-db.org/xquery/httpclient";
declare namespace atom = "http://www.w3.org/2005/Atom";
declare namespace dcterms = "http://purl.org/dc/terms/";
declare namespace opensearch = "http://a9.com/-/spec/opensearch/1.1/";
declare namespace xf="http://www.w3.org/2002/xforms";
declare namespace ev="http://www.w3.org/2001/xml-events";

let $q := request:get-parameter('q', 'mondale')

let $title := concat('Example of LOC search results with Update button for query="', $q, '".')

let $atom-query-string := concat('http://id.loc.gov/search/?q=', $q, '&amp;format=atom')

let $search-url := xs:anyURI($atom-query-string)

let $get := httpclient:get($search-url, false(), <params/>)
    
let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $style := <style/>

let $model :=
<xf:model id="ead-records">

<xf:instance id="loc-results"  xmlns:atom="http://www.w3.org/2005/Atom">
    <atom:feed>
        {for $entry in $get//atom:entry
          return 
          <atom:entry>
            <atom:title>{$entry/atom:title/text()}</atom:title>
            <atom:link>{$entry/atom:link[not(@type)]/@href/string()}</atom:link>           
            <atom:id>{$entry/atom:id/text()}</atom:id>    
            </atom:entry>
            }
    </atom:feed>
</xf:instance>
<xf:instance id="submit-params" xmlns="">
   <data>
      <title>abc</title>
      <id>def</id>
  </data>
</xf:instance>

<xf:instance id="repeat-submit-result" xmlns="">
    <events/>
</xf:instance>

<xf:submission id="repeat-submit" method="get" action="log-event.xq" replace="instance" instance="repeat-submit-result"
    ref="instance('submit-params')">
</xf:submission>
    
</xf:model>
    

let $content :=
<div class="content col-lg-12">
    <div class="col-lg-8">
        <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
             <tr>
                <th>Record Number</th>
                <th>Title</th>
                <th>Library of Congress ID</th>
                <th>Update</th>
             </tr>
           </thead>
           <tbody>
              <xf:repeat ref="instance('loc-results')/atom:entry" id="loc-repeat">
              <tr>
                  <th><xf:output value="position()"/></th>
                  <td><xf:output ref="atom:title"/></td>
                  <td>
                  <xf:trigger appearance="minimal" style="text-decoration:underline; color:blue;">
                    <xf:label><xf:output ref="atom:id"/>
                    </xf:label>
                    <xf:action ev:event="DOMActivate">
                      <xf:load show="new">
                        <xf:resource value="atom:link"/>
                      </xf:load>
                      </xf:action>
                      </xf:trigger>
                   </td>
                  <td>
                    <xf:trigger>
          				<xf:label>Update</xf:label>
          				<xf:action ev:event="DOMActivate">          				   
          				   <xf:setvalue ref="instance('submit-params')/title" value="instance('loc-results')/atom:entry[index('loc-repeat')]/title" />
          				   <xf:setvalue ref="instance('submit-params')/id" value="instance('loc-results')/atom:entry[index('loc-repeat')]/id" />
          				   <xf:send submission="repeat-submit"/>
          				</xf:action>
          			</xf:trigger>
        	       </td>
              </tr>
              </xf:repeat>
           </tbody>
        </table>  
        {if ($debug)
       then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
       else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
    }
    </div>
</div>

return style:assemble-form($title, $model, $style, $content, $debug)