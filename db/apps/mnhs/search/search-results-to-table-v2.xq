xquery version "1.0";

declare namespace httpclient="http://exist-db.org/xquery/httpclient";
declare namespace atom ="http://www.w3.org/2005/Atom";
declare namespace dcterms="http://purl.org/dc/terms/";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes indent=yes";

let $title := 'Search results in HTML table'

let $search-url := xs:anyURI('http://id.loc.gov/search/?q=mondale&amp;format=atom')

let $get := httpclient:get($search-url, false(), <params/>)

return
<html>
  <head>
     <title>{$title}</title>
  </head>
  <body>
    <h4>{$title}</h4>
    
    <!--
    <opensearch:totalResults xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">9</opensearch:totalResults>
<opensearch:startIndex xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">1</opensearch:startIndex>
<opensearch:itemsPerPage xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">20</opensearch:itemsPerPage>
    -->
    <table border="1">
       <thead>
         <tr>
            <th>Title</th>
            <th>ID</th>
            <th>Created</th>
            <th>Updated</th>
         </tr>
       </thead>
       <tbody>
          {for $entry in $get//atom:entry
          return
          <tr>
              <th style="text-align:left">{$entry/atom:title/text()}</th>
              <td><a href="{$entry/atom:link[not(@type)]/@href/string()}">{$entry/atom:id/text()}</a></td>
              <td>{substring($entry/dcterms:created/text(), 1, 10)}</td>
              <td>{substring($entry/atom:updated/text(), 1, 10)}</td>
          </tr>
          }
       </tbody>
    </table>
  </body>
</html>