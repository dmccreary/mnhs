xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace httpclient = "http://exist-db.org/xquery/httpclient";
declare namespace atom = "http://www.w3.org/2005/Atom";
declare namespace dcterms = "http://purl.org/dc/terms/";
declare namespace opensearch = "http://a9.com/-/spec/opensearch/1.1/";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes indent=yes";

let $q := request:get-parameter('q', 'mondale')

let $title := concat('LOC search results for query "', $q, '"')

let $atom-query-string := concat('http://id.loc.gov/search/?q=', $q, '&amp;format=atom')

let $search-url := xs:anyURI($atom-query-string)

let $get := httpclient:get($search-url, false(), <params/>)

(: note this is Bootstrap 3! :)
let $content :=
<div class="content col-lg-12">
    <!--
    <opensearch:totalResults xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">9</opensearch:totalResults>
<opensearch:startIndex xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">1</opensearch:startIndex>
<opensearch:itemsPerPage xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">20</opensearch:itemsPerPage>
    -->
    <div class="col-lg-8">
        <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
             <tr>
                <th>Title</th>
                <th>Library of Congress ID</th>
                <th>Created</th>
                <th>Updated</th>
             </tr>
           </thead>
           <tbody>
              {for $entry in $get//atom:entry
              return
              <tr>
                  <th style="text-align:left">{$entry/atom:title/text()}</th>
                  <td><a href="{$entry/atom:link[not(@type)]/@href/string()}">{$entry/atom:id/text()}</a></td>
                  <td>{substring($entry/dcterms:created/text(), 1, 10)}</td>
                  <td>{substring($entry/atom:updated/text(), 1, 10)}</td>
              </tr>
              }
           </tbody>
        </table>
    </div>
</div>

return style:assemble-page($title, $content)