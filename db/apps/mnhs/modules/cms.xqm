xquery version "3.0";

module namespace cms = "http://mnhs.org/cms";
(:
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";
:)
import module namespace config = "http://mnhs.org/config" at "config.xqm";

(: functions to modify the cms records 
<table name="eparties"><!-- Row 1 -->
    <tuple>
        <table name="CmsParty">
            <tuple>
                <atom name="irn" type="text" size="short">10127119</atom>
                <atom name="NamPartyType" type="text" size="short">Organization</atom>
                <atom name="SummaryData" type="text" size="short">A. A. Richardson Photo-Illustrating Company</atom>
                <atom name="BioBirthEarliestDate" type="text" size="short"/>
                <atom name="BioDeathEarliestDate" type="text" size="short"/>
                <atom name="AutAuthority" type="text" size="short">Library of Congress Name Authority File</atom>

:)

declare variable $cms:app-collection := '/db/apps/mnhs';
declare variable $cms:data-collection := concat($cms:app-collection, '/data-cms');

(: get a tuple with this irn :)
declare function cms:get-tuple($irn as xs:string) as node() {
collection($cms:data-collection)/table/tuple/table/tuple
  [./atom[@name='irn'] and
   ./atom/text() = $irn and
   ./atom = 'Library of Congress Name Authority File'
   ]
};

declare function cms:get-name-from-tuple($tuple as node()) as xs:string {
$tuple/atom[@name='SummaryData']
};

(: update the SummaryData which is the org name :)
declare function cms:update-tuple-summary-data($irn as xs:string, $new-name as xs:string) {
 let $tuple := cms:get-tuple($irn)
 return
    update value $tuple/atom[@name='SummaryData'] with $new-name 
};

(: update the AutAuthorityID with the new Library of Congress ID :)
declare function cms:update-tuple-id($irn as xs:string, $new-id as xs:string) {
 let $tuple := cms:get-tuple($irn)
 return
    update value $tuple/atom[@name='AutAuthorityID'] with $new-id 
};

(:
10141047|update-name|n86746137|Anchor Books 
10073857|update-name|n82081084|Anti-saloon League of America
:)
declare function cms:update-string-to-xml($input as xs:string) as node() {
let $tokens := tokenize($input, '\|')
return
(: we need at least 4 paramters to create a valid update :)
if (count($tokens) ge 4)
then
    <update>
       <input-string>{$input}</input-string>
       <irn>{$tokens[1]}</irn>
       <cmd>{$tokens[2]}</cmd>
       <new-id>{$tokens[3]}</new-id>
       <new-name>{cms:new-name-constructor($tokens[4]) }</new-name>
    </update>
else
    <error>
       <message>Error parsing pipe-delimited records in input: {$input}</message>
    </error>
};

declare function cms:append-update-to-pending-updates-file($update as node()) {
let $update-file-path := $config:cms-update-file-path
let $create-file-if-not-present :=
   if (doc-available($update-file-path))
      then ()
      else xmldb:store($config:cms-data-collection, $config:pending-update-file-name, <pending-updates/>)
let $update-file-doc := doc($update-file-path)/pending-updates
return
  update insert $update into $update-file-doc
};

(: here is the new name constructor function.  We add a period to the
   end if the last character is not a period :)
declare function cms:new-name-constructor($in-name as xs:string) as xs:string {
  let $last-char := substring($in-name, string-length($in-name), 1)
  return
     if ($last-char = '.')
       then $in-name
       (: concat($in-name, '.') :)
       else $in-name
};

(:  This function takes all the input parameters and appends
    update elements to the pending-updates.xml file for each paramter.
    Note that it returns a sequence of the update records for debugging.
:)
declare function cms:append-updates-from-input-parameters() as node()* {
    for $name in request:get-parameter-names()
        return
            if ($name eq 'num' or $name eq 'start' or $name eq 'debug')
             then ()
             else
                let $update-node := cms:update-string-to-xml($name)
                let $append := cms:append-update-to-pending-updates-file($update-node)
                return $update-node
};