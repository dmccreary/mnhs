xquery version "3.0";

module namespace config = "http://mnhs.org/config";
(:
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
:)

(: if you type in "$config:" you should be a prompt for any of the following variables 
Avoid the xmldb:exist:///db paths for users :)

declare variable $config:server-name := request:get-server-name(); (: eg. 'localhost' :)
declare variable $config:hostname := request:get-hostname(); (: the IP address :)
declare variable $config:context := request:get-context-path(); (: 'exist' or '/' :)
declare variable $config:port-number := request:get-server-port(); (: integer :)
declare variable $config:exist-host := 
  if ($config:port-number = 80)
    then concat('http://', $config:server-name)
    else concat('http://', $config:server-name, ':', $config:port-number);

declare variable $config:web-base := concat($config:exist-host, $config:context);

declare variable $config:app-id := 'mnhs';
declare variable $config:app-home-collection := concat('/db/apps/', $config:app-id);
declare variable $config:config-file-path := concat($config:app-home-collection, '/config.xml');
declare variable $config:config := doc($config:config-file-path)/config;

declare variable $config:app-version := $config:config/app-version/text();
declare variable $config:last-update-date := $config:config/last-update-date/text();

(: all the pending updates will go into this file :)
declare variable $config:pending-update-file-name := 'pending-updates.xml';

(: look for a default password, if there is none, use null :)
declare variable $config:admin-password :=
   if ($config:config/admin-password)
     then concat($config:app-home-collection, '/', $config:config/admin-password/text())
     else concat($config:app-home-collection, '');

(: CMS Specific :)
declare variable $config:cms-data := concat($config:app-home-collection, '/data-cms');
declare variable $config:cms-data-collection := concat($config:app-home-collection, '/data-cms');
declare variable $config:cms-max-records := xs:integer($config:config/cms-max-records/text());
declare variable $config:cms-export-file-path := $config:config/cms-export-file-path/text();
declare variable $config:cms-update-file-path := concat($config:cms-data, '/', $config:pending-update-file-name);
declare variable $config:cms-update-doc := doc($config:cms-update-file-path)/pending-updates;

(: a sequence of all the update records :)
declare variable $config:cms-updates := doc($config:cms-update-file-path)//update;
declare variable $config:cms-archive-collection := concat($config:app-home-collection, '/data-cms-update-archive');

(: EAD Specific :)

(: if the config file has an override use it, else default to data-ead :)
declare variable $config:ead-data-collection :=
   if ($config:config/ead-data-collection)
     then concat($config:app-home-collection, '/', $config:config/ead-data-collection/text())
     else concat($config:app-home-collection, '/data-ead');
(: alias for above :)
declare variable $config:ead-data := $config:ead-data-collection;

(: Relationship Designators :)
declare variable $config:code-table-collection := concat($config:app-home-collection, '/code-tables');
declare variable $config:relationship-designators-file-name := 'relationship-designators.xml';
declare variable $config:relationship-designators-file-path := concat($config:code-table-collection, '/', $config:relationship-designators-file-name);
(: this returns a sequence of strings, one for each relationship-designator :)
declare variable $config:relationship-designators := doc($config:relationship-designators-file-path)//item;

declare variable $config:ead-update-file-path := concat($config:ead-data-collection, '/', $config:pending-update-file-name);
declare variable $config:ead-export-dir := $config:config/ead-export-dir/text();
declare variable $config:ead-max-records := $config:config/ead-max-records/text();
(: we move update logs to this area with a timestamp :)
declare variable $config:ead-archive-collection := concat($config:app-home-collection, '/data-ead-update-archive');
declare variable $config:ead-possible-match-url-prefix := $config:config/ead-possible-match-url-prefix/text();
