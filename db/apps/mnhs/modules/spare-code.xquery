let $html-link := replace($id, 'info:lc', 'http://id.loc.gov')

<a href="{$html-link}" target="_blank">{$html-link}</a>