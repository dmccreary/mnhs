xquery version "3.0";

(: functions to aid in Library of Congress Name Authority Search :)

module namespace loc = "http://mnhs.org/loc-search";
(:
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
:)

import module namespace config = "http://mnhs.org/config" at "config.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";
declare namespace atom="http://www.w3.org/2005/Atom";
declare namespace opensearch="http://a9.com/-/spec/opensearch/1.1/";

declare variable $loc:base-uri := 'http://id.loc.gov/search/?';
declare variable $loc:name-authority-base-uri := 'http://id.loc.gov/authorities/names/';
declare variable $loc:base-uri-marcxml := 'http://id.loc.gov/authorities/names';
declare variable $loc:format := 'atom';
declare variable $loc:marcxml-format := '.marcxml.xml';
(: if you add this to the URL then the names search will ONLY search for authority names, :)
declare variable $loc:names-only-suffix := '&amp;q=cs%3Ahttp%3A%2F%2Fid.loc.gov%2Fauthorities%2Fnames';

(: the format is like this http://id.loc.gov/search/?q=mondale&amp;format=atom :)
declare function loc:create-html-uri($q as xs:string) as xs:anyURI {
   xs:anyURI(concat($loc:base-uri, 'q=', encode-for-uri($q)))
};

(: the format is like this http://id.loc.gov/search/?q=mondale&amp;format=atom :)
declare function loc:create-search-uri($q as xs:string) as xs:anyURI {
   xs:anyURI(concat($loc:base-uri, 'q=', encode-for-uri($q), $loc:names-only-suffix, '&amp;format=', $loc:format))
};
declare function loc:create-search-uri($q as xs:string, $start as xs:integer) as xs:anyURI {
   xs:anyURI(concat($loc:base-uri, 'q=', encode-for-uri($q), $loc:names-only-suffix, '&amp;format=', $loc:format, '&amp;start=', $start))
};
declare function loc:create-search-uri($q as xs:string, $start as xs:integer, $num as xs:integer) as xs:anyURI {
   xs:anyURI(concat($loc:base-uri, 'q=', encode-for-uri($q), $loc:names-only-suffix, '&amp;format=', $loc:format, '&amp;start=', $start, '&amp;count=', $num))
};

(: this runs an HTTP GET the query using the $URI 
   It returns the httpclient record with the result in the httpclient-body :)
declare function loc:run-query($uri as xs:anyURI) as node() {
   httpclient:get($uri, false(), <params/>)
};

(: This is our core first-level search.  It does a Library of Congress Name Search using the LOC service
   and returns an atom feed. 
   $q is a name of an organization, person or family.  :)
declare function loc:search($q as xs:string) as node() {
  let $search-uri := loc:create-search-uri($q)
  return loc:run-query($search-uri)
};
declare function loc:search($q as xs:string, $start as xs:integer) as node() {
  let $search-uri := loc:create-search-uri($q, $start)
  return loc:run-query($search-uri)
};
declare function loc:search($q as xs:string, $start as xs:integer, $num as xs:integer) as node() {
  let $search-uri := loc:create-search-uri($q, $start, $num)
  return loc:run-query($search-uri)
};

declare function loc:match-exact($q as xs:string) as node()? {
  let $run := loc:search($q)
  let $exact-match := $run//atom:entry[atom:title=$q]
  return $exact-match
};

declare function loc:id($q as xs:string) as xs:string? {
  let $run := loc:search($q)
  let $exact-match := $run//atom:entry[atom:title=$q]/atom:id/text()
  return $exact-match
};

declare function loc:test-api($q as xs:string) as node() {
  let $run := loc:search($q)
  let $possible-match-count := count($run//atom:entry)
  let $exact-match-id := $run//atom:entry[atom:title=$q]/atom:id/text()
  let $exact-match-name := $run//atom:entry[atom:title=$q]/atom:title/text()

  return
     <results>
        <possible-match-count>{$possible-match-count}</possible-match-count>
        <exact-match-id>{$exact-match-id}</exact-match-id>
        <exact-match-name>{$exact-match-name}</exact-match-name>
     </results>
};

declare function loc:approximate-match-xml($name as xs:string) as node() {
let $run := loc:search($name)
let $possible-match-count := count($run//atom:entry)
  return
  <div class="container">
    <table>
      {for $entry in $run//atom:entry
        return
          <tr>
            <td>{$entry/atom:title/text()}</td>
            <td><a href="{$entry/atom:link[not (@type)]/@href/string()}">Link to LOC</a></td>
          </tr>
          }
    </table>
  </div>
};

(: Given a sequence of strings, this does the markxml records for each string
   and returns a <marcrecords> wrapper with  a total count
   <marcrecords>
      <totalResults>25</totalResults>
      <marcxml:record>...</marcxml:record>
   <.marcrecords>
   This returns sequence of marcxml records element for a single input string.
   It first does a search on the library of congress web site and for
   each atom hit it gets the Marcxml file.
   Filter out all entries that are NOT name authorities :)
declare function loc:search-for-marcxml-ids($name as xs:string) as node()* {
  (: this returns an XML file in atom format with a sequence of atom entries :)
  let $run := loc:search($name)
  return
  <marcrecords>
    <!-- search-for-marcxml-ids() -->
    <totalResults>{$run//opensearch:totalResults/text()}</totalResults>
    { (: [starts-with(atom:link/@href, 'http://id.loc.gov/authorities/names/')] :)
    for $entry in $run//atom:entry
      let $id := loc:url-to-id($entry/atom:id/text()) 
      return
         loc:get-one-marcxml-with-id($id)
    }
</marcrecords>
};

declare function loc:search-for-marcxml-ids($name as xs:string, $start as xs:integer) as node()* {
  (: this returns an XML file in atom format with a sequence of atom entries :)
  let $run := loc:search($name, $start)
  return
  <marcrecords>
    <!-- search-for-marcxml-ids() -->
    <totalResults>{$run//opensearch:totalResults/text()}</totalResults>
    { (: [starts-with(atom:link/@href, 'http://id.loc.gov/authorities/names/')] :)
    for $entry in $run//atom:entry
      let $id := loc:url-to-id($entry/atom:id/text()) 
      return
         loc:get-one-marcxml-with-id($id)
    }
</marcrecords>
};

declare function loc:search-for-marcxml-ids($name as xs:string, $start as xs:integer, $num as xs:integer) as node()* {
  (: this returns an XML file in atom format with a sequence of atom entries :)
  let $run := loc:search($name, $start, $num)
  return
  <marcrecords>
    <!-- search-for-marcxml-ids() -->
    <totalResults>{$run//opensearch:totalResults/text()}</totalResults>
    { (: [starts-with(atom:link/@href, 'http://id.loc.gov/authorities/names/')] :)
    for $entry in $run//atom:entry
      let $id := loc:url-to-id($entry/atom:id/text()) 
      return
         loc:get-one-marcxml-with-id($id)
    }
</marcrecords>
};

(: These functions retrieve information from the LOC in marcxml format based on a LOC id:)

(: First we test for a LOC id number in the CMS file.  If we find a LOC number we use that to lookup the 
marcxml file.  If we do not find an LOC id then we use the name to lookup the marcxml file. :)
(: full path is $in/tuple/table/tuple/atom[@name ='AutAuthorityID'] ne '') :)

declare function loc:determine-search-type($cms-tuple as node()) as node()* {
if ($cms-tuple//atom[@name ='AutAuthorityID'] ne '')
  then
     <marcrecords>
        <!-- get-one-marcxml-with-id -->
        <totalResults>1</totalResults>
        {loc:get-one-marcxml-with-id($cms-tuple//atom[@name ='AutAuthorityID']/text())}
     </marcrecords>
  else loc:search-for-marcxml-ids($cms-tuple//atom[@name ='SummaryData']/text()) 
};



(: the format is like this http://id.loc.gov/authorities/names/n79018723.marcxml.xml:)
declare function loc:create-search-uri-for-marcxml($id as xs:string) as xs:anyURI {
   xs:anyURI(concat($loc:base-uri-marcxml, '/', encode-for-uri($id), $loc:marcxml-format))
};

(: we could filter out all nodes and get node :)
declare function loc:run-marcxml($id as xs:string) as node()? {
  let $lookup-uri := loc:create-search-uri-for-marcxml($id)
  let $unfiltered := loc:run-query($lookup-uri)//marcxml:record
  return
    loc:concept-match-filter($unfiltered)
};

(: give a single id, this will return the marcxml record from the LOC :)
declare function loc:get-one-marcxml-with-id($id as xs:string) as node()? {
  let $lookup-uri := loc:create-search-uri-for-marcxml($id)
  let $unfiltered := loc:run-query($lookup-uri)//marcxml:record
  (: loc:concept-match-filter($unfiltered) :)
  return
    $unfiltered
};

(: returns true if the record is undifferentiated :)
declare function loc:undifferentiated-name($marcxml as node()?) as xs:boolean {
if (substring($marcxml/marcxml:controlfield[@tag = '008'], 33, 1) = 'b')
  then true()
  else false()
};

(: returns TRUE if the name is the same as all the subfields in 100, 110 and 111 
   Note that we put spaces between the subfields :)
declare function loc:preferred-name-match-indicator($name as xs:string, $marcxml-rec as node()) as xs:boolean {
string-join($marcxml-rec/marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield, ' ') = $name
};

(: this is how we create a preferred name by joining the subfields of the records together :)
declare function loc:get-preferred-name-from-marc($marcrecord as element()) as xs:string {
string-join($marcrecord/marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield, ' ')
};

(: filters all marc xml records that don't match the concept filter
   Note that to work you MUST pass in a sequence of marcxml:record elements :)
declare function loc:concept-match-filter($marcrecords as node()*) as node()* {
for $r in $marcrecords
return
  if ((
       ($r/marcxml:datafield/@tag = '100' or 
        $r/marcxml:datafield/@tag = '110' or 
        $r/marcxml:datafield/@tag = '111')        
        and 
        ($r/marcxml:datafield/marcxml:subfield/@code = 't' or 
         $r/marcxml:datafield/marcxml:subfield/@code = 'k' or 
         $r/marcxml:datafield/marcxml:subfield/@code = 'v')
       )
       or $r/marcxml:datafield/@tag = '130')
      then () else $r     
};

declare function loc:non-preferred-name-match-indicator($name as xs:string, $marcxml-rec as node()) as xs:boolean {
$marcxml-rec/marcxml:datafield[@tag = '400' or @tag = '410' or @tag = '411']/marcxml:subfield = $name
};

declare function loc:insert-authfilenumber-attribute($element as element(), $id as xs:string) as node() {
let $update := update insert attribute authfilenumber {$id} into $element
let $file-name := util:document-name($element)
let $collection-name := util:collection-name($element)
let $full-path-name := concat($collection-name, '/', $file-name)
let $doc := doc($full-path-name)/*
return
$doc
};

(: given an input like: 'info:lc/authorities/names/n79080558' we return the string after the last slash 'n79080558' :)
declare function loc:url-to-id($in as xs:string) as xs:string {
let $tokens := tokenize($in, '/')
return $tokens[last()]
};

declare function loc:name-autority-id-to-uri($id as xs:string) as xs:string {
concat($loc:name-authority-base-uri, $id)
};

(: here is the input format
<marcxml:record xmlns:marcxml="http://www.loc.gov/MARC21/slim" xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:mets="http://www.loc.gov/METS/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <marcxml:leader>00838cz a2200205n 4500</marcxml:leader>
    <marcxml:controlfield tag="001">n79018723</marcxml:controlfield>
...
and here is what we return:

   http://id.loc.gov/authorities/names/n79018723.marcxml.xml
:)
declare function loc:get-locid-from-marcxml-record($markxml-record as node()) as xs:string {
$markxml-record//marcxml:controlfield[@tag='001']/text()
};

(: here is the input format
<marcxml:record xmlns:marcxml="http://www.loc.gov/MARC21/slim" xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:mets="http://www.loc.gov/METS/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <marcxml:leader>00838cz a2200205n 4500</marcxml:leader>
    <marcxml:controlfield tag="001">n79018723</marcxml:controlfield>
...
and here is what we return:

   http://id.loc.gov/authorities/names/n79018723.marcxml.xml
:)
declare function loc:get-uri-from-marcxml-record($markxml-record as node()) as xs:string {
let $id := $markxml-record/marcxml:controlfield[@tag='001']/text()
return loc:create-search-uri-for-marcxml($id)
};

(: makes a copy of the pending-updates.xml file and put it in the appropriate archive area 
   $system is either "cms' or 'ead' 
   returns the status as a full file-name path that was used to store the archive and an update count :)
declare function loc:archive-pending-updates($system as xs:string) as node() {

(: get the file name from the config file - typicall 'pending-updates.xml' :)
let $pending-file-name := $config:pending-update-file-name

let $current-data-collection :=
  if ($system = 'cms')
     then $config:cms-data
     else $config:ead-data
let $archive-collection-name :=
  if ($system = 'cms')
     then $config:cms-archive-collection
     else $config:ead-archive-collection

let $create-archive-if-not-present :=
   if (xmldb:collection-available($archive-collection-name))
     then ()
     else xmldb:create-collection($config:app-home-collection, concat('data-', $system, '-update-archive'))
     
let $pending-updates-file := concat($current-data-collection, '/', $pending-file-name)
let $date-time := current-dateTime()
(: we only store in the format "archive-2014-07-02T10-05-36.xml".
   Note that colons cannnot be used in the file name so we replace them with dashes and take only the first 19 chars of the datetime
   Assume 1 second between updates :)
let $archive-file-name := concat('archive-', replace(substring($date-time, 1, 19), ':', '-'), '.xml')
let $archive-file-path := concat($archive-collection-name, '/', $archive-file-name)

(: copy the pending-updates to the new file with the new name:)
let $pending-update-doc := doc($pending-updates-file)/pending-updates
let $update-count := count($pending-update-doc//update)
let $store := xmldb:store($archive-collection-name, $archive-file-name, $pending-update-doc)

(: this will effectivly clear the pending updates file of all update records! :)
let $store := xmldb:store($current-data-collection, $pending-file-name, <pending-updates/>)

return
   <archive-result>
      <user>{xmldb:get-current-user()}</user>
      <archive-file-path>{$archive-file-path}</archive-file-path>
      <update-count>{$update-count}</update-count>
   </archive-result>
};

(: Warning, this function DEPENDS on the correct DOCUMENT ORDER in the Marc XML files. :)
declare function loc:name-from-subfields($marcrecord as node()) as xs:string {
let $r := $marcrecord
let $name := $r/marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']
return
        string-join($name/marcxml:subfield, ' ')
};

(: this was an attempt to build the new names from subfields without depending
on document order.  The problems with patterns such as $a $c $d $c made this
not feasabile. :)
declare function loc:name-from-subfields-fixed-place($marcrecord as node()) as xs:string {
let $r := $marcrecord
let $name := $r/marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']
(: orginization or conference :)
let $org-or-conf :=
   if ($r/marcxml:datafield/@tag = '110' or $r/marcxml:datafield/@tag = '111')
      then true()
      else false()
return
   normalize-space(string-join((
        string-join($name/marcxml:subfield[@code = 'a'], ' '),
        string-join($name/marcxml:subfield[@code = 'q'], ' '),
        string-join($name/marcxml:subfield[@code = 'b'], ' '),
        string-join($name/marcxml:subfield[@code = 'n'], ' '),
        if ($org-or-conf)
          then 
            (string-join($name/marcxml:subfield[@code = 'd'], ' '),
            string-join($name/marcxml:subfield[@code = 'c'], ' '))
          else
            (string-join($name/marcxml:subfield[@code = 'c'], ' '),
            string-join($name/marcxml:subfield[@code = 'd'], ' '))
            ,
        string-join($name/marcxml:subfield[@code = 'e'], ' '),
        string-join($name/marcxml:subfield[@code = 'g'], ' '),
        string-join($name/marcxml:subfield[@code = 'j'], ' ')
        
    ), ' '))
};