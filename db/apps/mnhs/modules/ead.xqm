xquery version "3.0";

(: this is an OLD version used just for testing.  Use the list-ead-corp-names.xq for production. :)

module namespace ead = "http://mnhs.org/ead";
(:
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";
:)

import module namespace config = "http://mnhs.org/config" at "config.xqm";

(: functions to modify the ead records 
<ead audience="external">
    <eadheader audience="internal" findaidstatus="edited-full-draft" encodinganalog="MARC" scriptencoding="iso15924" dateencoding="iso8601" countryencoding="iso3166-1" repositoryencoding="iso15511" langencoding="iso639-2">
        <eadid countrycode="us" mainagencycode="MnHi">00003</eadid>
        ...
        <corpname encodinganalog="110" role="creator"> Alpha Center for Public/Private
					Initiatives, Inc. (Minneapolis, Minn.). </corpname>
					
		<corpname>Control Data Corporation<corpname>
	    ...
	    <persname encodinganalog="700">Ades, C. Douglas. </persname>
	    ...
	    <famname>Boschwitz family.</famname>
	    
	    The goal is to add Library of Congress Authority Name IDs
	    
	    <corpname authfilenumber="n80033969" source="lcnaf">Control Data Corporation<corpname>
	    
	    Sample Search
	    http://id.loc.gov/search/?q=Control+Data+Corporation&q=cs%3Ahttp%3A%2F%2Fid.loc.gov%2Fauthorities%2Fnames&format=atom
:)

declare variable $ead:app-collection := '/db/apps/mnhs';
declare variable $ead:data-collection := concat($ead:app-collection, '/data-ead');
declare variable $ead:test-data-collection := concat($ead:app-collection, '/data-ead');
declare variable $ead:names := collection($ead:data-collection)//(corpname|persname|famname);
declare variable $ead:count-names := count($ead:names);
declare variable $ead:corp-names := collection($ead:data-collection)//corpname;
declare variable $ead:pers-names := collection($ead:data-collection)//persname;
declare variable $ead:fam-names := collection($ead:data-collection)//famname;
declare variable $ead:names-with-sources := $ead:names[@source];
declare variable $ead:names-with-authfilenumber := $ead:names[@authfilenumber];

(: get a the element a file, element name and name-text 
   Note that element name will be 'corpname|persname|famname' 
   
   Note that because the source name may have MANY leading and trailing spaces we will return ANY that match the normalized comparison.  :)
declare function ead:get-name($file-path as xs:string, $element-name as xs:string, $old-name-text as xs:string) as element() {
let $file-doc := doc($file-path)/ead
let $elements-with-name := $file-doc//*[name(.) = $element-name]
(: note that we return it by comparing normalized space versions :)
let $this-element := $elements-with-name[normalize-space(.) = normalize-space($old-name-text)]
return
  if ($this-element)
  then $this-element
  else
     <error>
        <file-path>{$file-path}</file-path>
        <element-name>{$element-name}</element-name>
        <old-name>{$old-name-text}</old-name>
     </error>
};

(: the EAD update string has the following format 
file-path|element-name|old-name|new-id|new-name|
returns
<update>
   <file-path></file-path>
   <element-name></element-name>
   <old-name><old-name>
   <new-id></new-id>
   <new-name></new-name>
</update>
:)
declare function ead:update-string-to-xml($input as xs:string) as node() {
let $tokens := tokenize($input, '\|')
return
<update>
   <input-string>{$input}</input-string>
   <file-path>{$tokens[1]}</file-path>
   <element-name>{$tokens[2]}</element-name>
   <old-name>{$tokens[3]}</old-name>
   <new-id>{$tokens[4]}</new-id>
   <new-name>{ead:new-name-constructor($tokens[5])}</new-name>
</update>
};

(: input is the $update record, output is null :)
declare function ead:append-update-to-pending-updates-file($update as node()) {
let $update-file-path := $config:ead-update-file-path
let $create-file-if-not-present :=
   if (doc-available($update-file-path))
      then ()
      else xmldb:store($config:ead-data-collection, $config:pending-update-file-name, <pending-updates/>)
let $update-file-doc := doc($update-file-path)/pending-updates
return
  update insert $update into $update-file-doc
};

(: Update the name with a new authfilenumber and  @source = source=”lcnaf” 
    The format of the update and is insert is
      update value element/@attribute with value
      update insert attribute name {'value'} into element
    See http://exist-db.org/exist/apps/doc/update_ext.xml
    :)
declare function ead:update-name-element($name-element as element(), $authfilenumber as xs:string, $new-name as xs:string) as element() {
    (: if the source attribute already exists, then update it, else insert a new one :)
    let $insert-or-update-source :=
       if ($name-element/@source)
       then
          update value $name-element/@source with 'lcnaf'
       else
          update insert attribute source {'lcnaf'} into $name-element
    
    (: if the authfilenumber attribute already exists, then update it, else insert a new one :)
    let $insert-or-update-authfilenumber :=
       if ($name-element/@authfilenumber)
       then
          update value $name-element/@authfilenumber with $authfilenumber
       else
          update insert attribute authfilenumber {$authfilenumber} into $name-element
          
     let $update-name :=
       update value $name-element with $new-name
         return $name-element
};

(:
run this update
<update>
    <file-path>/db/apps/mnhs/data-ead-test/00000.xml</file-path>
    <element-name>corpname</element-name>
    <old-name>Washburn High School (Minneapolis, Minn.)</old-name>
    <new-id>n85807225</new-id>
    <new-name>Washburn High School (Minneapolis, Minn.)</new-name>
</update>
:)

declare function ead:run-update($update as node()) as xs:string {
let $file-path := $update/file-path/text()
let $element-name := $update/element-name/text()
let $old-name := $update/old-name/text()
let $new-id := $update/new-id/text()
let $new-name := $update/new-name/text()

(: this should point to an in-memory node :)
let $ead-file-root := doc($file-path)/ead

(: get the actual element we want to update here - the predicate is fast and the conditional trims down the extra :)
let $name-element-to-update := $ead-file-root//(persname|corpname|famname)
   [
    (name(.) = $element-name)
    and (normalize-space(./text()) eq normalize-space($old-name))
   ]

return
if ($name-element-to-update)
  then
  (: no checks for existing attributes here :)
     let $update1 := update insert attribute source {'lcnaf'} into $name-element-to-update
     let $update2 := update insert attribute authfilenumber {$new-id} into $name-element-to-update
     let $update3 := update value $name-element-to-update with $new-name
     return
        concat('Success: Element "', $element-name, '" with name "', $old-name, '" has been updated in file: ', $file-path)
  else
     concat('Error: Element "', $element-name, '" with name "', $old-name, '" was NOT found in in file: ', $file-path)
(:
let $insert-or-update-source :=
   if ($name-element/@source)
    then
       update value $name-element/@source with 'lcnaf'
    else
       update insert attribute source {'lcnaf'} into $name-element

(: if the authfilenumber attribute already exists, then update it, else insert a new one :)
let $insert-or-update-authfilenumber :=
   if ($name-element/@authfilenumber)
     then
        update value $name-element/@authfilenumber with $update/new-id
     else
        update insert attribute authfilenumber {$update/new-id} into $name-element
 
 let $update-name :=
   update value $name-element with $update/new-name
:)
};

(: both the ead-list-names and the ead-possible-match must do the same processing :)
declare function ead:prep-name-to-send-to-loc($input-name as xs:string) as xs:string {
let $strip-extra-spaces := normalize-space($input-name)
let $remove-trailing-period := replace($strip-extra-spaces, '\.$', '')
let $remove-relationships := ead:remote-relationship-designator($remove-trailing-period)
return $remove-relationships
};

(:  This function takes all the input parameters and appends
    update elements to the pending-updates.xml file for each paramter.
    Note that it returns a sequence of the update records for debugging.
:)
declare function ead:append-updates-from-input-parameters() as node()* {
    for $name in request:get-parameter-names()
        return
            if ($name eq 'num' or $name eq 'start' or $name eq 'debug')
             then ()
             else
                let $update-node := ead:update-string-to-xml($name)
                let $append := ead:append-update-to-pending-updates-file($update-node)
                return $update-node
};

(: here is the new name constructor function.  We add a period to the
   end of the LOC name if the last character is not a period :)
declare function ead:new-name-constructor($in-name as xs:string) as xs:string {
  let $last-char := substring($in-name, string-length($in-name), 1)
  return
     if ($last-char = '.' or $last-char = ')' or $last-char = '-')
       then $in-name
       else concat($in-name, '.')
};

(: extract the relationship from the name if it has one or null:)
declare function ead:extract-relationship-designator($input as xs:string) as xs:string {
let $relionships := $config:relationship-designators
return
    string-join(
         for $rel in $relionships
             (: look for comma space or dash space before the relationship 
                 note that we could use concat('[,-] ', $rel, '$') if we were SURE that the relationship was at the very end:)
             let $pattern := concat('[,-] ', $rel)
             return
               if (matches($input, $pattern))
                    then $rel
                     else ()
      , '')
};

(: remove the relationship string from the name :)
declare function ead:remote-relationship-designator($input as xs:string) as xs:string {
let $relionships := $config:relationship-designators

(: this will return the comma-space, a dash-space relationship or nothing :)
let $match-relationship :=
   string-join(
                for $rel in $relionships
                    let $add-comma-space := concat(', ', $rel)
                    let $add-dash-space := concat('- ', $rel)
                    return
                      if (matches($input, $add-comma-space))
                           then $add-comma-space
                         else if (matches($input, $add-dash-space))
                            (: here the string that will be removed is just the space rel, not the - space rel :)
                            then concat(' ', $rel)
                            else ()
        , '')
return
     (: if we find ANY relaitionship then we remove it - else we return the original input :)
     if ($match-relationship)
          then replace($input, $match-relationship, '')
          else $input
};