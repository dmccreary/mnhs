xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "modules/style.xqm";
import module namespace config = "http://mnhs.org/config" at "modules/config.xqm";

let $title := 'Minnesota Historical Society Linked Name Application'

(: 

<h4>Metrics</h4>
     <a href="views/metrics.xq">DC Forms Metrics</a> Metrics of overall tests<br/>
     <a href="views/source-code-metrics.xq">XQuery Source Code Metrics</a> Metrics of the number of modules, functions for test and refactoring<br/>
     
     <h4>Admin Tools</h4>
     <a href="admin/index.xq">Admin Index</a><br/>
     <a href="views/view-config.xq">View config file based on static and dynamic context</a><br/>
     
     <h4>Search Tools</h4>
     
:)
let $content :=
<div class="content">
     <p>This application matches corporate, family and personal names to the Library of Congress Name Authority File 
     and updates preferred access points and authority record identifiers in CMS or EAD files.  This application also
     allows you to view and select possible matches for names that do not result in an exact match with a
     Library of Congress name
     authority record.</p>
     
     Application Version: <b>{$config:app-version}</b> last updated on {$config:last-update-date}<br/>
     
     <h4>CMS Links</h4>
        <a href="views/list-names-from-cms.xq">List All CMS Files</a> List of data files from CMS in data-2 collection and match results<br/>      
        <a href="views/cms-list-pending-updates.xq?format=html">List of CMS Updates HTML</a> HTML table that has a list of updates to CMS files<br/>
        
      <h5>Export</h5>
      <a href="views/cms-list-pending-updates.xq?format=tab">List of CMS Updates Tab-Delimited ASCII file</a> tab delimited text file.<br/>
      <a href="views/cms-list-pending-updates.xq?format=csv">List of CMS Updates in CSV file</a> CSV file with quotes around fields<br/>
      
      <h5>Archive</h5>
      <a href="scripts/cms-archive-pending-updates.xq?format=csv">Archive CMS Pending Updates</a> move the pending updates to an archive location.<br/>
      <!--
         <a href="scripts/cms-export-csv-data.xq">Export CMS Updates to File System</a>
          CMS Export File Path: {$config:cms-export-file-path}
     -->
     
     <br/>
     <h4>EAD Links</h4>
       <a href="views/list-ead-names.xq">List All Names in EAD Files</a> List names in EAD files and Library of Congress Name Authority lookups.<br/>
       <a href="views/ead-list-pending-updates.xq?format=html">List All EAD Pending Updates (HTML)</a> List of the pending updates to the EAD files in HTML table<br/>
       <a href="views/ead-list-pending-updates.xq?format=csv">List All EAD Pending Updates (CSV)</a> List of the pending updates to the EAD files in a CSV file<br/>
    <h5>Export</h5>
     <a href="scripts/export-ead-data.xq">Export EAD Files</a> export EAD files based on export dir in config file.
      ({$config:ead-export-dir}) <br/>
     
      <h5>EAD Code Tables</h5>
      <a href="views/list-relationship-designators.xq">Relationship Designators</a> A list of relationships that must be removed from EAD names before compared with the Library of Congress names.

     <br/>
     <h4>Tests</h4>
     <a href="unit-tests/marc-filter-test.xq">Concept Filter</a> Test for filtering out (removing) various Marcxml records<br/>
     <a href="unit-tests/undifferentiated-name-test-html.xq">Undifferentiated Name Test</a> Test for filtering out (removing) various Marcxml records<br/>
     <a href="unit-tests/index.xq">All Unit Tests</a> Listing of all unit tests in reverse chronological order<br/>
     
     <h5>Useful Links</h5>
     <a href="{$config:web-base}">eXist dashboard</a>Used to load new packages<br/>
     <a href="{$config:web-base}/admin">eXist admin</a> used to verify system version and memory config<br/>
     <a href="{$config:web-base}/status">eXist status page</a> detailed status for remote debugging<br/>
     <a href="http://id.loc.gov">US Library of Congress Linked Data Service for Authorities and Vocabularies</a><br/>
     <a href="http://id.loc.gov">US Library of Congress Linked Data Service for Authorities and Vocabularies</a><br/>
     
     <a href="search/index.xq">Sample LOC Search Service Tests</a> Tests of the Library of Congresss name search functions.<br/>
     
     <h5>Quick Lists</h5>
     <a href="views/list-names-from-cms.xq?num=10">CMS Quick List</a> list only 10 CMS names<br/>
     <a href="views/list-ead-names.xq?num=10">EAD Quick List</a> list only 10 EAD names<br/>
        
     
     <br/>
     <p>Please contact <a href="mailto:annmkelly1@gmail">Ann Kelly</a> if you have any feedback on this app.</p>
</div>

return style:assemble-page($title, $content)