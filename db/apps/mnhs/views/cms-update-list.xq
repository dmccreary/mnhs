xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";
import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";

let $title := 'List Pending Updates'
let $format := request:get-parameter('format', 'csv')

(: we get the path name to the update file from the main config module so we have a central point of change :)
let $update-doc := $config:cms-update-doc

let $table :=
<table class="table table-striped table-bordered table-hover table-condensed">
    <tr>
      <th>#</th>
      <th>IRN</th>
      <th>Current Name</th>
      <th>Current Auth ID</th>
      <th>Change</th>
      <th>LOC ID</th>
      <th>New Name</th>
    </tr>
    {for $update at $count in $update-doc/update
       let $irn := $update/irn/text()
       let $cms-record := cms:get-tuple($irn)
       return
          <tr>
             <td>{$count}</td>
             <td>{$update/irn/text()}</td>
             <td>{$cms-record/atom[@name='SummaryData']/text()}</td>
             <td>{$cms-record/atom[@name='AutAuthorityID']/text()}</td>
             <td>{$update/cmd/text()}</td>
             <td>{$update/locid/text()}</td>
             <td>{$update/newname/text()}</td>
          </tr>
    }
</table>

return
if ($format = 'csv')
   then
      util2:table-to-csv($table)
   else 
      style:assemble-page($title, <div class="content">{$table}</div>)

