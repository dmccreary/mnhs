xquery version "1.0";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

(: echo-post.xq: Return all data from an HTTP post to the caller. :)

declare namespace exist = "http://exist.sourceforge.net/NS/exist";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";

declare option exist:serialize "method=xml media-type=text/xml indent=yes";

(: we assume each checkbox has a hidden field that has the id followed by a dash and the new name to be used 
<input type="checkbox" name="{$irn-number}" id="{$irn-number}"/>,
<input type="hidden" name="{$irn-number}-{$new-name}" />
:)
let $update-list :=
<update-list>
{
       for $parameter in request:get-parameter-names()
       let $irn := substring-before($parameter, '-')
       let $suffix := substring-after($parameter, '-')
       let $cmd :=
          if (starts-with($suffix, 'insert-new-id-'))
             then
                'insert-new-id'
             else
                'update-name'
       let $new-id := substring-after($suffix, 'insert-new-id-')
       let $name := substring-after($suffix, 'update-name-')
       (: Remove actual execution for now
       let $executed :=
         if ($cmd = 'insert-new-id')
             then cms:update-tuple-id($irn, $new-id)
             else if ($cmd = 'update-name')
               then cms:update-tuple-summary-data($irn, $name)
               else 'unknown command'
               
            :)
       return
          <change>
            <in>{$parameter}</in>
            <irn>{$irn}</irn>
            <cmd>{$cmd}</cmd>
            <new-id>{$new-id}</new-id>
            <name>{$name}</name>
            
          </change>
}
</update-list>

return
<results>
  {$update-list}
</results>
