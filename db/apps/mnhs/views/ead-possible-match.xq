xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";
declare namespace opensearch="http://a9.com/-/spec/opensearch/1.1/";

let $page-title := 'Possible Match EAD'

(: the collection name relative to the mnhs app :)
let $collection-name := request:get-parameter('collection', '/db/apps/mnhs/ead-data')

let $file-name := request:get-parameter('file', '')
(: not used any more
let $base-file-name := replace($file-name, '\.xml$', '')
:)
let $ead-data-collection := $config:ead-data-collection
let $file-path := concat($ead-data-collection, '/', $file-name)

(: Here is the name string we will use in our query.  We assume the name is URI encoded. :)
let $element-name := request:get-parameter('element-name', '')

(: we must have a non-cononlicalied URI encoded name here even if it has trailing spaces!!! :)
let $name := request:get-parameter('name', '')

let $start := xs:integer(request:get-parameter('start', 1))
(: get the default max number of records to fetched from the config file :)
let $num := xs:integer(request:get-parameter('num', $config:ead-max-records))

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $name-sent-to-loc-search := ead:prep-name-to-send-to-loc($name)
(: this is where we do the search and conversion to marcxml records :)
let $marcrecords-root := loc:search-for-marcxml-ids($name-sent-to-loc-search, $start, $num)

(: The total number of records we need to paginate through :)
let $total-loc-records := xs:integer($marcrecords-root/totalResults/text())
let $marcrecords := $marcrecords-root/marcxml:record

let $filtered-marcxml := loc:concept-match-filter($marcrecords)
let $pre-filter-count := count($marcrecords)
let $post-filter-count := count($filtered-marcxml)

let $sorted-records :=
  for $record in $filtered-marcxml
    let $sort-key := $record/marcxml:datafield[@tag='100' or @tag='110']/marcxml:subfield[@code="a"]/text()
   order by $sort-key
return
  (: loc:concept-match-filter( :)
  $record

let $content :=
<div class="content">
       EAD File ID = <a href="{$config:ead-possible-match-url-prefix}/{$file-name}" target="_blank">{$config:ead-possible-match-url-prefix}/{$file-name}</a><br/>
       Name Searched = <b>'{$name}'</b><br/>
       Element Name = <b>{$element-name}</b><br/>
       Start = <b>{$start}</b> Num = <b>{$num}</b><br/>
       Total LOC Record Count <b>{$total-loc-records}</b><br/>
       Pre Filter Count = <b>{$post-filter-count}</b> Post Filter Count = <b>{$post-filter-count}</b>
       <form method="post" action="../scripts/ead-add-pending-updates.xq">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>#</th>
              <th>Name</th>
              <th>Undiff</th>
              <th>Link</th>
              <th>Update</th>
           </tr>
        </thead>
        <tbody>{
          for $record at $count in $sorted-records
             let $title := loc:get-preferred-name-from-marc($record)
             let $new-id := $record/marcxml:controlfield[@tag='001']/text()
             
             let $new-name-a :=
               $record//marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield[@code='a']/text()
          
          (: this is really all the subfields in document order that are not a with spaces between them:)
          let $new-name-not-a :=
          string-join(
            $record//marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield[@code ne 'a']/text()
           , ' ')
          
          (: the new name must have a space between the a and b names :)
          
          let $new-name := concat($new-name-a, ' ', $new-name-not-a)
          return
             <tr>
                 <td>{$count + $start - 1}</td>
                 <td>
                    <b>{$new-name-a}</b>{' '}{$new-name-not-a}
                 </td>
                 <td>{if (loc:undifferentiated-name($record))
                         then <span class="text-danger">Undiff</span>
                         else ()
                     }
                 </td>
                 <td>
                    {if ($new-id)
                       then <a href="{loc:name-autority-id-to-uri($new-id)}" target="_blank">{$new-id}</a>
                       else ()
                    }
                 </td>
                 <td>
                     { '' (: file-path|element-name|old-name|new-id|new-name :) }
                     <input type="checkbox" name="{$file-path}|{$element-name}|{$name}|{$new-id}|{$new-name}|"/>
                 </td>
             </tr> 
       }</tbody>
    </table>
       {
         if ($start > 1)
            then
               <a class="button" href="{request:get-uri()}?start={$start - $num}&amp;name={$name}&amp;">Get Previous {$num} Records</a> 
            else ()
       }
       {' '}
       { (: only show the next button if we have more filtered rows then $num :)
       if ($total-loc-records gt $num)
         then
       <a class="button" href="{request:get-uri()}?start={$start + $num}&amp;name={$name}&amp;">Get additional records</a>
         else ()
       }
       <button type="submit">Add to Pending Updates</button>
     </form>
     <a href="{request:get-uri()}?debug=true&amp;collection={$collection-name}&amp;file={$file-name}&amp;element-name={$element-name}&amp;name={$name}">debug</a>
</div>

return
if ($debug)
  then
  <marcrecords>
     <input>
        <collection-name>{$collection-name}</collection-name>
        <file-name>{$file-name}</file-name>
        <element-name>{$element-name}</element-name>
        <name>{$name}</name>
        <start>{$start}</start>
        <num>{$num}</num>
        <total-loc-records>{$total-loc-records}</total-loc-records>
        <pre-filter-count>{$pre-filter-count}</pre-filter-count>
        <post-filter-count>{$post-filter-count}</post-filter-count>
     </input>
       {$marcrecords}
   </marcrecords>
  else style:assemble-page($page-title, $content)