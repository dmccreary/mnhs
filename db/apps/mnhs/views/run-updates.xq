xquery version "1.0";

(: echo-post.xq: Return all data from an HTTP post to the caller. :)

declare namespace exist = "http://exist.sourceforge.net/NS/exist";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";

declare option exist:serialize "method=xml media-type=text/xml indent=yes";

(: we assume each checkbox has a hidden field that has the id followed by a dash and the new name to be used 
<input type="checkbox" name="{$irn-number}" id="{$irn-number}"/>,
<input type="hidden" name="{$irn-number}-{$new-name}" />
:)
let $update-list :=
<update-list>
   <desc>The following IRN records will be updated with the new a new name.</desc>
    {  (: get all the parameter names, but remove all that don't have a checkbox on :)
       for $parameter1 in request:get-parameter-names()
       return
          if (request:get-parameter($parameter1, 'off') = 'on')
            then
                for $parameter2 in request:get-parameter-names()
                    return
                    if (starts-with($parameter2, concat($parameter1, '-')))
                       then
                          <update>
                            <irn>{substring-before($parameter2, '-')}</irn>
                            <new-name>{substring-after($parameter2, '-')}</new-name>
                           </update>
                       else ()

             else ()
    }
</update-list>

return $update-list