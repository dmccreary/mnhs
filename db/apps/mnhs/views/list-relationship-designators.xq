xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";

let $title := 'List Relationship Designators'

(: look up the path in the config file :)
let $items := doc($config:relationship-designators-file-path)//item

let $content :=
<div class="content">
   Relationship Designators Count: {count($items)}
   <div class="col-md-4">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>Relationship Designator</th>
           </tr>
        </thead>
        <tbody>{
          for $item at $count in $items
             order by $item
          return
             <tr>
                 <td>{$item/text()}</td>
             </tr> 
       }</tbody>
       </table>
       </div>
</div>

return style:assemble-page($title, $content)