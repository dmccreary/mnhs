xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";

let $app-collection := $style:db-path-to-app
let $data-collection := $config:ead-data-collection

let $file := request:get-parameter('file', '')
let $file-path := concat($data-collection, '/', $file)
let $file-doc := doc($file-path)/ead
let $update := update value $file-doc/eadheader/eadid with $file
return 
<results>ok</results>
