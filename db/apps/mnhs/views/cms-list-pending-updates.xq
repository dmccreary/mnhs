xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";
import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";

let $title := 'List CMS Pending Updates'
let $format := request:get-parameter('format', 'html')

(: we get the path name to the update file from the main config module so we have a central point of change :)
let $updates := doc($config:cms-update-file-path)//update
let $has-updates :=
   if ($updates)
      then true()
      else false()

(: The format of the update record is the following
<update>
    <file-path>/db/apps/mnhs/data-ead-test/00000.xml</file-path>
    <element-name>corpname</element-name>
    <old-name>Washburn High School (Minneapolis, Minn.)</old-name>
    <new-id>n85807225</new-id>
    <new-name>Washburn High School (Minneapolis, Minn.)</new-name>
</update>
:)


let $table :=
if (not($has-updates)) then ()
else
<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
        <tr>
           <th colspan="5" class="border-right">Current CMS Fields</th>
           <th colspan="2">Update Fields</th>
        </tr>
        <tr>
          <th>#</th>
          <th>IRN</th>
          <th>Command</th>
          <th>Current Name</th>
          <th class="border-right">Current ID</th>
          <th>New Name</th>
          <th>New ID</th>
        </tr>
    </thead>
    <tbody>
        {for $update at $count in $updates
           return
                let $irn := $update/irn/text()
                let $tuple := cms:get-tuple($irn)
                let $current-id := $tuple/atom[@name='AutAuthorityID']/text()
                let $current-name := cms:get-name-from-tuple($tuple)
                return
                   <tr>
                     <th>{$count}</th>
                     <td>{$update/irn/text()}</td>
                     <td>{$update/cmd/text()}</td>
                     <td>{$current-name}</td>
                     <td>
                        {if ($current-id)
                          then
                             (attribute {'class'} {'border-right'}, $current-id)
                          else
                            (attribute {'class'} {'border-right gray-text'}, 'none')
                         }
                     </td>
                     <td>{$update/new-name/text()}</td>
                     <td>{$update/new-id/text()}</td>
                   </tr>
        }
    </tbody>
</table>

let $content :=
  if ($has-updates)
     then
        <div class="content">
           {$table}
           <!-- <a href="../scripts/cms-run-updates.xq">RUN UPDATES</a> -->
           
           { (: if we are showing one format, create a link to the other format :)
             if ($format = 'html')
              then
                 (<a href="{request:get-uri()}?format=csv">CSV</a>,
                  ' ',
                 <a href="{request:get-uri()}?format=tab">Tab</a>
                 )
              else <a href="{request:get-uri()}?format=html">HTML Table</a>
           }
        </div>
    else
      <div class="content">
        There are no pending updates in {$config:cms-update-file-path}
      </div>
return
if ($format = 'csv')
   then
      util2:table-to-csv($table)
   else if ($format = 'tab')
      then util2:table-to-tab-delimited($table, 'csv-pending-updates.txt')
   else 
      style:assemble-page($title, $content)

