xquery version "1.0";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'List CMS Records with Library of Congress as Name Authority File'

let $start := xs:integer(request:get-parameter('start', 1))
(: make the default number of records to fetch be the number from the config file
   <cms-max-records>100</cms-max-records>
:)
let $num := xs:integer(request:get-parameter('num', $config:cms-max-records))
let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $app-collection := $style:db-path-to-app
let $data-collection := concat($app-collection, '/data-cms')

(: this takes all the checked lines and appends an update record to the pending-updates.xml file for each checked line 
   Note that passing NO input parameters means that the parameters all come in via the HTTP headers
   This function ignores start, num and debug parameters
   :)
let $append-updates := cms:append-updates-from-input-parameters()

(: get only the tuples that have both an AutAhuroity and a text of 'Library of Congress Name Authority File' 
<tuple>
    <atom name="irn" type="text" size="short">10127127</atom>
    <atom name="NamPartyType" type="text" size="short">Organization</atom>
    <atom name="SummaryData" type="text" size="short">A. Pearson Company</atom>
    <atom name="AutAuthority" type="text" size="short">Library of Congress Name Authority File</atom>
    <atom name="AutAuthorityID" type="text" size="short"/>
    <atom name="AutDisplayName" type="text" size="short"/>
</tuple>
:)
let $tuples := collection($data-collection)/table/tuple/table/tuple[./atom[./@name='AutAuthority' and ./text() ='Library of Congress Name Authority File']]

let $tuple-count := count($tuples)

(: the upper bound count is the minimum of the start + $num -1 OR the $tuple-count, whichever is lower :)
let $upper-bound := min(($start + $num - 1, $tuple-count))

(: the action can be this same script if you pass hidden parameters for the $start and $num
   or you can run a script that does the appends directory or with an intermediate confirmation.
   Use one of the following three:
   
   <form method="post" action="list-names-from-cms.xq">
   <form method="post" action="../scripts/cms-add-pending-updates.xq">
   <form method="post" action="/cms-confirm-updates.xq">
   
   :)
let $content :=
<div class="content">
   Showing records {$start} to {$upper-bound} of {$tuple-count}
   <form method="post" action="../scripts/cms-add-pending-updates.xq">
   <table class="table table-striped table-bordered table-hover table-condensed">
       <thead>
                <tr>
                   <th colspan="5" class="border-right">CMS Fields</th>
                   <th colspan="5">Library of Congress Search Results</th>
                </tr>
                <tr>
                   <th>#</th>
                   <th>Name</th>
                   <th>Party Type</th>
                   <th>CMS IRN</th>
                   <th class="border-right">Authority ID</th>
                   
                   <!-- <td>loc-results</td> -->
                   <th>Match Count</th>
                   <th>Exact Match ID</th>
                   <th>Undiff</th>
                   <th>Preferred Name</th>
                   <th>Update</th>
                </tr>
     </thead>
     <tbody>{
       for $organization-tuple at $count in subsequence($tuples , $start, $num)
          let $party-type := $organization-tuple/atom[@name='NamPartyType']/text()
          let $irn-number := $organization-tuple/atom[@name='irn']/text()
          let $name-text := $organization-tuple/atom[@name='SummaryData']/text()
          let $cononicalized-name := normalize-space($name-text)
          (:    <atom name="AutAuthority" type="text" size="short">Library of Congress Name Authority File</atom>
                <atom name="AutAuthorityID" type="text" size="short"/>
          :)
          let $current-authority-id := $organization-tuple/atom[@name='AutAuthorityID']/text()
          let $current-authority-present :=
             if (string-length($current-authority-id) gt 0)
                then true()
                else false()      
          
          (: this returns a <marcrecords> :)
          let $loc-search-results := loc:determine-search-type($organization-tuple)
          
          (: this returns a Marcrecord for each found item or one if we have an ID :)
          let $determine-search-type := $loc-search-results/marcxml:record
          
          let $total := $loc-search-results/totalResults/text()
          
          (: There could be multipe names here (a and b names) and they need to be seperatd by a space.
             The new name algorithm assumes the Marc Record sub=fields are in document order.  
             Note that this includes 130 names where the others do not.  :)
          let $new-name :=
            string-join(
              $determine-search-type//marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111' or @tag = '130']/marcxml:subfield
              , ' ')


          let $marcxml-record-count := count($determine-search-type)
          
          let $new-single-lcid :=
             if ($marcxml-record-count = 1)
                then
                  $determine-search-type/marcxml:controlfield[@tag='001']/text()
                else ''
             
         let $names-different :=
            $marcxml-record-count = 1 and not(loc:preferred-name-match-indicator($cononicalized-name, $determine-search-type))
         
         (: we set this flag if we have a current ID and it is not the same as the LOC id :)
         let $ids-different :=
           if ($current-authority-present and normalize-space($current-authority-id) != normalize-space($new-single-lcid) )
             then true()
             else false()
          return
             <tr>
                 <th>{$start + $count - 1}</th>
                 <td>{$name-text}</td>
                 <td>{$party-type}</td>
                 <td>{$irn-number}</td>
                 
                 <td class="border-right">
                    { '' (: Current Authority ID :)}
                    {if ($current-authority-present)
                       then <a href="http://id.loc.gov/authorities/names/{$current-authority-id}" target="_blank">{$current-authority-id}</a>
                       else ''
                     } 
                 </td>
                 <!--
                 <td> loc-results 
                 {$loc-search-results}
                 </td>-->
                 
                 <td>
                 <!-- match count -->
                 { (: Match Count
                     This column is a number Or Link to LOC if Match Count is 1 or if gt 1 then go to the preferred match screen
                     Open in a new window. note that the name is not URI encoded and we may need to do this later :)
                  if ($marcxml-record-count eq 0)
                     then <span class="gray-text">0</span>
                     else if ($marcxml-record-count eq 1)
                        then
                           if ($current-authority-id)
                              then <a href="{concat($loc:name-authority-base-uri, $current-authority-id)}" target="_blank">1</a>
                              else
                                 let $new-loc-id := $determine-search-type/marcxml:controlfield[@tag='001']/text()
                                 return
                                    if (string-length($new-loc-id) gt 1)
                                      then 
                                        <a href="http://id.loc.gov/authorities/names/{$new-loc-id}" target="_blank">1</a>
                                      else
                                         <span class="danger">{$determine-search-type}</span>
                     else (: gt 1 - note that names might have an ampersand in them so they must be encoded to be %26 :)
                        <a href="cms-possible-match.xq?name={encode-for-uri($cononicalized-name)}&amp;irn={$irn-number}" target="_blank">
                           {$total}
                        </a>
                 }
                 </td>
                 
                 <td>
                    { (: Exact Match ID if there is one and only-one match then we show an ID in this column :)
                    if($marcxml-record-count = 1)
                      then
                            if ($current-authority-id)
                               then
                                 <a href="{loc:name-autority-id-to-uri($current-authority-id)}" target="_blank">{$current-authority-id}</a>
                                 (: /marcxml:record/marcxml:controlfield[@tag='001']/text() :)
                               else
                                 let $new-loc-id := $determine-search-type/marcxml:controlfield[@tag='001']/text()
                                 return
                                    if (string-length($new-loc-id) gt 1)
                                      then 
                                        <a href="http://id.loc.gov/authorities/names/{$new-loc-id}" target="_blank">{$new-loc-id}</a>
                                      else
                                         <span class="text-error">{$determine-search-type}</span>
                      else
                         (: we have 0 or many IDs so don't show them  :)
                         ()
                    }
                 </td>
                 <td>
                    { (: undifferentiated name :)
                      if ( $marcxml-record-count = 1 and loc:undifferentiated-name($determine-search-type) )
                        then
                           <span class="text-danger">undiff</span>
                        else ()
                    }
                 </td>
                 
                 <td>
                 { (: Preferred Name :)
                 if ($names-different)
                    then
                       $new-name
                    else if ($marcxml-record-count = 1)
                       then <span class="text-light-gray">same</span>
                       else ()
                 }
                 </td>
                 
                 <td>
                   { (: update checkbox :)
                     if ($marcxml-record-count eq 1
                      and ($names-different or (not($current-authority-present) or $ids-different) ) )
                      then
                        let $insert-id :=
                            if ($current-authority-present)
                              then ()
                              else 'insert-id'
                        let $update-name :=
                            if ($names-different)
                              then 'update-name'
                              else ()
                        let $cmd := concat($insert-id, $update-name)
                        return
                        (
                          <input type="checkbox" name="{$irn-number}|{$cmd}|{$new-single-lcid}|{$new-name}"/>,
                          if ($insert-id) then <span class="text-light-gray">ii</span> else (),
                          if ($update-name) then <span class="text-light-gray">un</span> else ()
                        )
                      else ()
                   }
                   
                 </td>
             </tr> 
          }</tbody>
       </table>
       <input type="hidden" name="num" value="{$num}"/>
       <input type="hidden" name="debug" value="{$debug}"/>
       <input type="hidden" name="start" value="{$start}"/>
       <button class="btn btn-primary float-right" type="submit">Add checked items to pending updates</button><br/>
     </form>
     
     <table style="float:right;">
        <tr>
          <td colspan="2" style="text-align:center;">
             <a class="button btn list-pending" href="../views/cms-list-pending-updates.xq" target="_blank">List Pending</a> {' '}
          </td>
        </tr>
        <tr>
          <td>{ (: only show the previous button if we are at least $num into the sequence :)
            if ($start > $num)
             then
                <a class="button btn prev" href="{request:get-uri()}?start={$start - $num}&amp;num={$num}">&lt; Previous</a>
             else <span style="width:80px; color: silver;">&lt; Previous</span>
            }
          </td>
          <td>
          {  (: only show the next if we have at least $start + $num under the total count :)
             if ($start + $num < $tuple-count)
             then
             <a class="button btn next" href="{request:get-uri()}?start={$start + $num}&amp;num={$num}">Next &gt;</a>
             else ()
          }
          </td>
        </tr>
     </table>
</div>

return style:assemble-page($title, $content)