xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";
declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $page-title := 'Possible Match'
(: we will assume the name is URI encoded :)
let $name := request:get-parameter('name', '')
let $irn := request:get-parameter('irn', '')

(: by default we start at 1, if this parameter is present we add the start to the LOC search :)
let $start := xs:integer(request:get-parameter('start', 1))

(: get the default number of records to fetch from the config file.
   note that the name of the LOC service for the max record is "count" :)
let $num := xs:integer(request:get-parameter('num', $config:cms-max-records))

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

(: this returns 
<marcrecords>
    <!-- search-for-marcxml-ids() -->
    <totalResults>{$run//opensearch:totalResults/text()}</totalResults>
    ...sequnce of marcrecords
    <marcxml:record>...<marcxml:record>
    <marcxml:record>...<marcxml:record>
</marcrecords>
 :)

let $current-tuple := cms:get-tuple($irn)
let $current-authority-file-id := $current-tuple/atom[@name='AutAuthorityID']/text()
let $loc-marcrecords := loc:search-for-marcxml-ids($name, $start, $num)

let $total-loc-records := xs:integer($loc-marcrecords/totalResults/text())

let $unfiltered-marcxml-records := $loc-marcrecords/marcxml:record
let $filtered-marcxml := loc:concept-match-filter($unfiltered-marcxml-records)

let $pre-filter-count := count($unfiltered-marcxml-records)
let $post-filter-count := count($filtered-marcxml)

(: this is the smaller of the max records or what we have left after the concept filter :)
let $min-next-record-count := min(( $post-filter-count, $num - $post-filter-count ))

let $sorted-records :=
  for $record in $filtered-marcxml
      let $primary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="a"]/text(), '-')
      let $secondary-names := string-join($record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code="b"]/text(), '-')
      let $full-title := concat($primary-names, $secondary-names)
      order by $full-title
      return $record

(: not doing start and end feedback 
       Sorted Items = {count($sorted-records)}
       :)
let $content :=
<div class="content">
       <span class="text-gray">Name Search = </span><b>'{$name}'</b><br/>
       
       <span class="text-gray">IRN = </span><b>{$irn}</b><br/>
       
       {if ($current-authority-file-id)
          then
             ('Current Authority File ID = ', $current-authority-file-id, <br/>)
          else ()
       }
       <span class="text-gray"> Total LOC records matching this query=</span><b>{$total-loc-records}</b>
       <span class="text-gray"> Starting Record=</span><b>{$start}</b><br/>
       
       <span class="text-gray"> Pre Concept Filter Count=</span><b>{$pre-filter-count}</b>
       <span class="text-gray"> Post Concept Filter Count=</span><b>{$post-filter-count}</b>
       <br/>
       
       <form method="post" action="../scripts/cms-add-pending-updates.xq">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>#</th>
              <th>Name</th>
              <th>Undiff</th>
              <th>Link</th>
              <th>Update</th>
           </tr>
        </thead>
        <tbody>{
          for $record at $count in $sorted-records
             let $a-title := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code='a']/text()
             let $not-a-title := $record/marcxml:datafield[@tag='100' or @tag='110' or @tag='111' or @tag='130']/marcxml:subfield[@code ne 'a']/text()
             (: add a space between the two titles :)
             let $new-title := concat(string-join($a-title, ''), ' ', string-join($not-a-title, ''))
             
             let $new-loc-id := $record/marcxml:controlfield[@tag='001']/text()

          return
             <tr>
                 <td>{$count + $start - 1}</td>
                 <td><b>{$a-title}</b>{' '}{$not-a-title}</td>
                 <td>{if (loc:undifferentiated-name($record))
                         then <span class="text-danger">Undiff</span>
                         else ()
                     }
                 </td>
                 <td>{ (: Link :)
                    if (string-length($new-loc-id) gt 1)
                       then <a href="{loc:name-autority-id-to-uri($new-loc-id)}" target="_blank">{$new-loc-id}</a>
                       else ()
                    }
                 </td>
                 <td>
                     { (: update :)
                     let $insert-id :=
                            if ($current-authority-file-id)
                              then ()
                              else 'insert-id'
                        let $update-name :=
                            if ($name = $new-title)
                              then ()
                              else 'update-name'
                        let $cmd := concat($insert-id, $update-name)
                     return
                        (
                          <input type="checkbox" name="{$irn}|{$cmd}|{$new-loc-id}|{$new-title}"/>,
                          if ($insert-id) then <span class="text-light-gray">ii</span> else (),
                          if ($update-name) then <span class="text-light-gray">un</span> else ()
                        )
                     }
                 </td>
                 
             </tr> 
       }</tbody>
       </table>
       
       
       <button type="submit">Add checked item to pending updates</button><br/>
       
       {
         if ($start > 1)
            then
               <a class="btn button prev" href="{request:get-uri()}?start={$start - $num}&amp;name={$name}&amp;irn={$irn}">Refetch Previous Records</a> 
            else ()
       }
       {' '}
       
       { (: only show the next button if we have more filtered rows then $num :)
       if ($total-loc-records gt ($start + $num) )
         then
           <a class="btn button next" href="{request:get-uri()}?start={$start + $num}&amp;name={$name}&amp;irn={$irn}&amp;num={$num}">
              Get additional Records starting at {$start + $num}
          </a>
         else ()
       }
     </form>
     <a href="{request:get-uri()}?debug=true&amp;name={$name}&amp;irn={$irn}&amp;start={$start}">debug</a>
</div>

return
if ($debug)
  then
  <marcrecords xmlns:marcxml="http://www.loc.gov/MARC21/slim" 
               xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#" 
               xmlns:xlink="http://www.w3.org/1999/xlink" 
               xmlns:ri="http://id.loc.gov/ontologies/RecordInfo#" 
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
               xmlns:mets="http://www.loc.gov/METS/" 
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <input>
        <name>{$name}</name>
        <irn>{$irn}</irn>
        <start>{$start}</start>
        <num>{$num}</num>
     </input>
     <output>
       <pre-concept-filter-count>{$pre-filter-count}</pre-concept-filter-count>
       <post-concept-filter-count>{$post-filter-count}</post-concept-filter-count>
       <!-- the unfiltered marc records-->
       {$unfiltered-marcxml-records}
     </output>
</marcrecords>
  else style:assemble-page($page-title, $content)