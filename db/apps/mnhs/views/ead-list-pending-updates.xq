xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";
import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";

let $title := 'List EAD Pending Updates'
let $format := request:get-parameter('format', 'html')

(: we get the path name to the update file from the main config module so we have a central point of change :)
let $updates := doc($config:ead-update-file-path)//update

(: The format of the update record is the following
<update>
    <file-path>/db/apps/mnhs/data-ead-test/00000.xml</file-path>
    <element-name>corpname</element-name>
    <old-name>Washburn High School (Minneapolis, Minn.)</old-name>
    <new-id>n85807225</new-id>
    <new-name>Washburn High School (Minneapolis, Minn.)</new-name>
</update>
:)

let $table :=
<table class="table table-striped table-bordered table-hover table-condensed">
   <thead>
    <tr>
      <th>#</th>
      <th>File</th>
      <th>Element Name</th>
      <th>Current Name</th>  
      <th>New ID</th>
      <th>New Name</th>
      <th class="border-right">Last Char</th>
    </tr>
    </thead>
    <tbody>
    {for $update at $count in $updates
       let $new-name := normalize-space($update/new-name)
       let $last-char-in-new-name := substring($new-name, string-length($new-name), 1)
       (: todo if the last char of the new name is already is punctuation then don't add it again :)
       return
          <tr>
             <td>{$count}</td>
             <td>{tokenize($update/file-path/text(), '/')[last()]}</td>
             <td>{$update/element-name/text()}</td>
             <td class="border-right">{$update/old-name/text()}</td>
             
             <td>{$update/new-id/text()}</td>
             <td>{$update/new-name/text() }</td>
             <td><span style="font-family:courier;size:large;font-weight:bold;">{$last-char-in-new-name}</span></td>
          </tr>
    }
    </tbody>
</table>

let $content :=
<div class="content">

   {$table}
   <a href="../scripts/ead-run-updates.xq">RUN UPDATES</a>
</div>

return
if ($format = 'csv')
   then
      util2:table-to-csv($table)
   else 
      style:assemble-page($title, $content)

