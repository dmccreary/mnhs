xquery version "1.0";

(: this is an old version just used for testing :)

import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

let $title := 'List Corp Names'

let $app-collection := $style:db-path-to-app
let $data-collection := concat($app-collection, '/data')

let $content :=
<div class="content">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>#</th>
              <th>File Name</th>
              <th>Encoding</th>
              <th>Role</th>
              <th>Org Name</th>
              <th>Match Count</th>
              <th>Exact Match ID</th>
           </tr>
        </thead>
        <tbody>{
          for $corpname-element at $count in collection($data-collection)//corpname
          let $name-text := $corpname-element/text()
          let $cononicalized-name := normalize-space(replace($name-text, '\.', ''))
          let $approximate-match := loc:approximate-match($cononicalized-name)
          return
             <tr>
                 <th>{$count}</th>
                 <td>{util:document-name($corpname-element)}</td>
                 <td>{$corpname-element/@encodinganalog/string()}</td>
                 <td>{$corpname-element/@role/string()}</td>
                 <td>{$name-text}</td>
                 <td>{$approximate-match/possible-match-count/text()}</td>
                 <td>{$approximate-match/exact-match-id/text()}</td>
             </tr> 
       }</tbody></table>
  
</div>

return style:assemble-page($title, $content)