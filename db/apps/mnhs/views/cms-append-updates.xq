xquery version "1.0";
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

let $title := 'Append Updates'

(: we get the path name to the update file from the main config module so we have a central point of change :)
let $update-doc := $config:cms-update-doc

(: we assume each checkbox has a hidden field that has the id followed by a dash and the new name to be used 
<input type="checkbox" name="{$irn-number}" id="{$irn-number}"/>,
<input type="hidden" name="{$irn-number}-{$new-name}" />
:)
let $update-list :=
<update-list>
{
   for $parameter in request:get-parameter-names()
       let $irn := substring-before($parameter, '-')
       let $suffix := substring-after($parameter, '-')
       let $cmd :=
          if (starts-with($suffix, 'insert-new-id-'))
             then
                'insert-new-id'
             else
                'update-name'
       let $new-id := substring-after($suffix, 'insert-new-id-')
       let $name := substring-after($suffix, 'update-name-')
       (: Remove actual execution for now
       let $executed :=
         if ($cmd = 'insert-new-id')
             then cms:update-tuple-id($irn, $new-id)
             else if ($cmd = 'update-name')
               then cms:update-tuple-summary-data($irn, $name)
               else 'unknown command'
               
            :)
       let $update-record :=
           <update>
                <in>{$parameter}</in>
                <irn>{$irn}</irn>
                <cmd>{$cmd}</cmd>
                {if ($cmd='insert-new-id')
                   then 
                     <locid>{$new-id}</locid>
                   else
                     <newname>{$name}</newname>
            }
          </update>
          let $append :=
             update insert $update-record into $update-doc
          return
             $update-record
}
</update-list>

let $content :=
<div class="content">
   The following command have been appended to the update list:
   <table class="table table-striped table-bordered table-hover table-condensed">
      <thead>
         <tr>
           <th>IRN</th>
           <th>CMD</th>
           <th>New ID</th>
           <th>New Name</th>
         </tr>
      </thead>
      <tbody>
        {for $update in $update-list/update
         return
           <tr>
              <td>{$update/irn/text()}</td>
              <td>{$update/cmd/text()}</td>
              <td>{$update/locid/text()}</td>
              <td>{$update/newname/text()}</td>
           </tr>
        }
    </tbody>
   </table>
</div>

return style:assemble-page($title, $content)
