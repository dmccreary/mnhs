xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";

declare namespace atom="http://www.w3.org/2005/Atom";

let $page-title := 'Possible Match'
let $name := request:get-parameter('name', '')
let $irn := request:get-parameter('irn', '')

(: TODO check for required parameter :)

(:
<entry>
    <title>Alaska Area Native Health Service. Nutrition Section</title>
    <link rel="alternate" href="http://id.loc.gov/authorities/names/n85042243"/>
    <link rel="alternate" type="application/rdf+xml" href="http://id.loc.gov/authorities/names/n85042243.rdf"/>
    <link rel="alternate" type="application/json" href="http://id.loc.gov/authorities/names/n85042243.json"/>
    <id>info:lc/authorities/names/n85042243</id>
    <author>
    <name>Library of Congress</name>
    </author>
    <updated>2008-06-22T00:00:00-04:00</updated>
    <dcterms:created xmlns:dcterms="http://purl.org/dc/terms/">1985-06-19T00:00:00-04:00</dcterms:created>
</entry>
:)

(: [starts-with(atom:link/@href, 'http://id.loc.gov/authorities/names/')] :)
let $loc-search := loc:search($name)//atom:entry[starts-with(atom:link/@href, 'http://id.loc.gov/authorities/names/')]

let $sorted-entries :=
  for $entry in $loc-search
   order by $entry/atom:title/text()
return $entry

let $content :=
<div class="content">
       Name = {$name}<br/>
       IRN = {$irn}<br/>
       <form method="post" action="run-updates-id-and-name.xq">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>#</th>
              <th>Name</th>
              <th>Link</th>
              <th>Update</th>
           </tr>
        </thead>
        <tbody>{
          for $entry at $count in $sorted-entries
             let $title := $entry/atom:title/text()
             let $id-uri := $entry/atom:id/text()
             let $id := loc:url-to-id($id-uri)
             let $link := $entry/atom:link[not(@type)]/@href/string()
          return
             <tr>
                 <td>{$count}</td>
                 <td>{$title}</td>
                 <td><a href="{$link}">{$link}</a></td>
                 <td>
                     <input type="checkbox" name="{$id}"/>
                     <input type="hidden" name="{$id}-{$irn}-{$title}" />
                 </td>
             </tr> 
       }</tbody></table>
       <button type="submit">Run updates</button>
     </form>
     {$loc-search}
</div>

return style:assemble-page($page-title, $content)
