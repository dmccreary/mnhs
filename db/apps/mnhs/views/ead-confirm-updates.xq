xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace cms = "http://mnhs.org/cms" at "../modules/cms.xqm";

(: Get a list of update command from a table-based form that has checkboxes in a update column 
   Incomming data must be in request:get-parameter-names() :)

let $debug := xs:boolean(request:get-parameter('debug', 'true'))

let $title := 'EAD Confirm Updates'

(: we assume each checkbox has a hidden field that has the id followed by a dash and the new name to be used 
<input type="checkbox" name="{$irn-number}""/>,
<input type="hidden" name="{$irn-number}-insert-id-{$new-name}" />,
<input type="hidden" name="{$irn-number}-update-name-{$new-name}" />
:)
let $update-list :=
<update-list>
   <query>{request:get-uri()}</query>
   <desc>The record will be updated.</desc>
   <all-names>
      {for $name in request:get-parameter-names()
       return
       <param>
          <name>{$name}</name>
          <value>{request:get-parameter($name, '')}</value>
       </param>
      }
   </all-names>
    {  (: get all the parameter names, but remove all that don't have a checkbox on :)
       for $parameter1 in request:get-parameter-names()
       return
          if (request:get-parameter($parameter1, 'off') = 'on')
            then
                for $parameter2 in request:get-parameter-names()
                    return
                    if (starts-with($parameter2, concat($parameter1, '-')))
                       then
                          <update>
                            <irn>{substring-before($parameter2, '-')}</irn>
                            <change>{substring-after($parameter2, '-')}</change>
                           </update>
                       else ()

             else ()
    }
</update-list>

let $content :=
<div class="content">
   Confirm Changes to IRN = {$update-list/update[1]/irn/text()}
   <form method="post" action="cms-append-updates.xq">
       <table class="table table-striped table-bordered table-hover table-condensed">
          <thead>
             <tr>
                <th>IRN</th>
                <th>Summary Data</th>
                <th>Change</th>
             </tr>
          </thead>
         {for $update in $update-list/update
            let $irn := $update/irn/text()
            let $tuple := cms:get-tuple($irn)
            (: The format of the strings in the <change> element is
                insert-new-id-n50023560
                update-name-A.J. Reach Co.
              :)
            let $new-name := substring-after($update/change/text(),'update-name-')
            let $new-loc-id := substring-after($update/change/text(),'insert-new-id-')
            return
               <tr>
                 <th>{$irn}</th>
                 <td>{ $tuple/atom[@name='SummaryData']/text() }</td>
                 <td>
                   { 
                   if ( contains($update/change/text(), 'insert-new-id') )
                      then
                        concat('Insert New Authority ID=', $new-loc-id)
                      else
                        concat("Update Name='", $new-name, "'" )
                   }
                   { (: put hidden names in inputs :)
                   if (contains($update/change, 'insert-new-id'))
                     then
                        <input type="hidden" name="{$irn}-insert-new-id-{$new-loc-id}" />
                     else if (contains($update/change, 'update-name'))
                        then
                          <input type="hidden" name="{$irn}-update-name-{$new-name}" />
                        else ()
                   }
                 </td>
               </tr>
         }
       </table>
       <button type="submit">Add to CMS Updates</button>
   </form>
</div>

return if ($debug)
  then $update-list
  else style:assemble-page($title, $content)