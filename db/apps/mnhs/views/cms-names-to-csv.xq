xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace util2 = "http://danmccreary.com/util2" at "../modules/util2.xqm";
(: declare option exist:serialize "method=text media-type=text/csv omit-xml-declaration=yes"; :)
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=yes";

let $app-collection := $style:db-path-to-app
let $data-collection := concat($app-collection, '/data-cms')
let $format := request:get-parameter('format', 'csv')

let $content :=
<div class="content">
       <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
           <tr>
              <th>#</th>
              <th>Org Name</th>
              <th>Authority Name</th>
              <th>Authority ID</th>
              <th>Nr Matches</th>
              <th>ID Match</th>
           </tr>
        </thead>
        <tbody>{
          for $organization-tuple at $count in 
             subsequence(collection($data-collection)/table/tuple/table/tuple
                [./atom/@name='NamPartyType' and (./atom/text()='Organization' or ./atom/text()='Person')]
                , 1, 10)
          let $name-text := $organization-tuple/atom[@name='SummaryData']/text()
          let $cononicalized-name := normalize-space($name-text)
          let $authority-name := $organization-tuple/atom[@name='AutAuthority']/text()
          let $current-id := $organization-tuple/atom[@name='AutAuthorityID']/text()
          let $approximate-match := loc:approximate-match-xml($cononicalized-name)
          return
             <tr>
                 <th>{$count}</th>
                 <td>{$name-text}</td>
                 <td>{$authority-name}</td>
                 <td>{$current-id}</td>
                 <td>{$approximate-match/possible-match-count/text()}</td>
                 <td>{$approximate-match/exact-match-id/text()}</td>
             </tr> 
       }</tbody></table>
  
</div>

return
  if ($format = 'csv')
  then util2:table-to-csv($content)
  else $content