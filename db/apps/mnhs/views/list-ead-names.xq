xquery version "1.0";
import module namespace style='http://danmccreary.com/style' at '../modules/style.xqm';
import module namespace loc = "http://mnhs.org/loc-search" at "../modules/loc-search.xqm";
import module namespace ead = "http://mnhs.org/ead" at "../modules/ead.xqm";
import module namespace config = "http://mnhs.org/config" at "../modules/config.xqm";

declare namespace marcxml="http://www.loc.gov/MARC21/slim";

let $title := 'List Names in EAD Files'

let $app-collection := $style:db-path-to-app
let $data-collection := $config:ead-data-collection

let $start := xs:integer(request:get-parameter('start', 1))
(: get the default :)
let $num := xs:integer(request:get-parameter('num', $config:ead-max-records))
let $debug := xs:boolean(request:get-parameter('debug', 'false'))

(: this line takes all the checkbox input parameters and appends a list of <update> elements to the pending-updates.xml file :)
let $append-updates-records := ead:append-updates-from-input-parameters()

(: get corpnames that are not child elements of the repository element :)
let $corpnames := collection($data-collection)//(corpname[name(..) ne 'repository']|persname|famname)

let $corpnames-count := count($corpnames)

let $content :=
<div class="content">
    Listing all name element in {$data-collection}<br/>
    Found {count($corpnames)} names<br/>
    Showing records {$start} to {$start + $num - 1}
    <form method="post" action="../scripts/ead-add-pending-updates.xq">
    <table class="table table-striped table-bordered table-hover table-condensed">
           <thead>
               <tr>
                   <th colspan="5" class="border-right">EAD Fields</th>
                   <th colspan="5">Library of Congress Search Results</th>
               </tr>
                 <tr>
                    <th>#</th>
                    <th>File ID</th>
                    <th>EAD Element Name</th>
                    <th>EAD Name</th>
                    <th class="border-right">EAD authfilenumber</th>
                    
                    <th>Match Count</th>
                    <th>LOC ID</th>
                    <th>Undiff</th>
                    <th>Preferred Name</th>
                    <th>Update</th>
                 </tr>
           </thead>
        <tbody>
        {
          for $name-element at $count in subsequence($corpnames, $start, $num)
            let $element-name := name($name-element)
            
            let $collection-name := util:collection-name($name-element)
            let $full-file-name := util:document-name($name-element)
            let $file-path := concat($collection-name, '/', $full-file-name)
            (: the non-cononicalized name with trailing spaces included!!! :)
            let $name-text := $name-element/text()
            let $current-authfilenumber := $name-element/@authfilenumber/string()
            (: Remove any trailing characters - this is what we use for searching the LOC site :)
            
            let $name-sent-to-loc-search := ead:prep-name-to-send-to-loc($name-text)
            
            (: this goes through ALL the relationship values and returns a relationship if it finds it in the name :)
            let $relationship := ead:extract-relationship-designator($name-text)
            
            (: this returns a marcrecords, one for each atom entry match :)
            let $marcrecords-root := loc:search-for-marcxml-ids($name-sent-to-loc-search)
            let $total-records := $marcrecords-root/totalResults
            let $marcrecords := $marcrecords-root/marcxml:record
            let $marcxml-record-count := count($marcrecords)
            (: IF we have a single record, this is the LOC id such as n80033969 :)
            let $loc-id :=
               if ($marcxml-record-count = 1)
                  then loc:get-locid-from-marcxml-record($marcrecords)
                  else ()
            
            
            let $exact-match-id :=
               if ($marcxml-record-count = 1)
                  then
                     (: if we have a single record then go to the LOC HTML page for that ID
                     :)
                     <a href="{concat($loc:name-authority-base-uri, $loc-id)}" target="_blank">{$loc-id}</a>
                  else ()
                  
            (: There could be many names here (a, b, c, d, etc. and they may repeat.) 
               The path expression just gets them in document order.  They each need to be seperatd by a space.
               This result places all sub-fields in the document order. :)
            let $new-name :=
                string-join(
                   $marcrecords//marcxml:datafield[@tag = '100' or @tag = '110' or @tag = '111']/marcxml:subfield
                  , ' ')
              
            let $names-different :=
               $marcxml-record-count = 1 and 
               not(loc:preferred-name-match-indicator($name-text, $marcrecords))

            return
               <tr>
                   <th>{$count + $start - 1}</th>
                   <td>{
                      
                      (: remove any trailing .xml extension :)
                      let $file-base-name := replace($full-file-name, '\.xml$', '')
                      let $eadid := replace($name-element/ancestor::ead/eadheader/eadid/text(), '\.xml$', '')
                      return
                         if ($file-base-name = $eadid)
                            then $eadid
                            else
                               (
                                  <span class="text-error">{$file-base-name}!={$eadid}</span>
                                  ,' ',
                                  <a href="fix-eadid.xq?file={$full-file-name}">fix</a>
                               )
                      }</td>
                   <td>{$element-name}</td>
                   
                   <td>{$name-text}</td>
                   
                   <td class="border-right">{$current-authfilenumber}</td>
                   <td>
                    { (: Match Count
                        This column is a number Or Link to LOC if Match Count is 1 or if gt 1 then go to the preferred match screen
                        Open in a new window. note that the name is not URI encoded and we may need to do this later :)
                     if ($current-authfilenumber)
                       then 
                          <a href="{concat($loc:name-authority-base-uri, $current-authfilenumber)}" target="_blank">1</a>
                     else if ($marcxml-record-count eq 0)
                        then '0'
                        else if ($marcxml-record-count eq 1 and $exact-match-id)
                           then
                             <a href="{concat($loc:name-authority-base-uri, $loc-id)}" target="_blank">1</a>
                        else
                        
                        (: gt 1 - note that names might have an ampersand in them so they must be encoded to be %26 
                           Note that we are doing a cononicalization of the name before we pass the parameter to the possible match
                           since the name may contain MANY spaces and newlines in the EAD files.
                        :)
                           <a href="ead-possible-match.xq?file={util:document-name($name-element)}&amp;element-name={$element-name}&amp;name={encode-for-uri(normalize-space($name-text))}" target="_blank">
                              {$total-records}
                           </a>
                    }
                   </td>
                   <td>
                    { (: Exact Match ID :)
                      $exact-match-id
                    }
                   </td>
                    <td>{ (: undifferentiated names column :)
                        let $this-marc-record := $marcrecords[marcxml:controlfield[@tag='001']/text() = $current-authfilenumber]
                        return
                        if ($current-authfilenumber)
                          then 
                                if (loc:undifferentiated-name($this-marc-record))
                                  then
                                    <span class="text-danger">undiff</span>
                                  else ()
                           else
                                if ( $marcxml-record-count = 1 and loc:undifferentiated-name($this-marc-record) )
                                  then <span class="text-danger">undiff</span>
                                  else ()
                        }
                     </td>
                   <td>
                     { (: Preferred Name :)
                        if ($names-different)
                           then
                              $new-name
                           else ()
                        }
                   </td>
                   <td>
                     { (: If we have a record count AND the current authfilenumber not present
                          and if we do have an authfilenumber it not the same as the LOC id then add the checkbox to the column :)
                       if (($marcxml-record-count = 1)
                            and not($current-authfilenumber and $current-authfilenumber eq $loc-id)
                            or $names-different)
                        then
                           (: file-path|element-name|old-name|new-id|new-name| 
                              note that if we have a relationship designator in the original name
                              then we add this relationship (after a comma-space) to the new LOC name
                           :)
                           <input type="checkbox" name="{$file-path}|{$element-name}|{$name-text}|{$loc-id}|{$new-name}{if($relationship)
                              then concat(', ', $relationship)
                              else ()
                           }"/>
                        else ()
                     }
                   </td>
               </tr> 
       }
      </tbody>
   </table>
      <input type="hidden" name="num" value="{$num}"/>
      <input type="hidden" name="debug" value="{$debug}"/>
      <input type="hidden" name="start" value="{$start}"/>
      
      <button type="submit"  class="btn btn-primary float-right">Add checked items to pending updates</button><br/>
   </form>
   
   <table style="float:right;">
        <tr>
          <td colspan="2" style="text-align:center;">
             <a class="button btn list-pending" href="../views/ead-list-pending-updates.xq" target="_blank">List Pending</a> {' '}
          </td>
        </tr>
        <tr>
          <td>{ (: only show the previous button if we are at least $num into the sequence :)
            if ($start > $num)
             then
                <a class="button btn prev" href="{request:get-uri()}?start={$start - $num}&amp;num={$num}">&lt; Previous</a>
             else <span style="width:80px; color: silver;">&lt; Previous</span>
            }
          </td>
          <td>
          {  (: only show the next if we have at least $start + $num under the total count :)
             if ($start + $num < $corpnames-count)
             then
             <a class="button btn next" href="{request:get-uri()}?start={$start + $num}&amp;num={$num}">Next &gt;</a>
             else ()
          }
          </td>
        </tr>
     </table>
  
</div>

return style:assemble-page($title, $content)