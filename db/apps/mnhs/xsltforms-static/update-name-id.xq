xquery version "1.0";

let $name := request:get-parameter('name', '')

let $id := request:get-parameter('id', '')

let $log := util:log-system-out( concat('log-event name=', $name, ' id=', $id) )

(: put in code that updates a named entity with a new id :)
return
<events>
   <event>{current-dateTime()}</event>
   <name>{$name}</name>
   <id>{$id}</id>
</events>