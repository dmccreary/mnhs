xquery version "1.0";

let $debug := xs:boolean(request:get-parameter('debug', 'true'))

let $form :=
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xf="http://www.w3.org/2002/xforms">
    <head>
        <title>Hello World in XForms</title>
        <xf:model>
            <xf:instance>
                <data xmlns="">
                    <PersonGivenName/>
                </data>
            </xf:instance>
        </xf:model>
    </head>
    <body>
        <p>Test of XSLT forms raw assembler using XQuery to create PI and debug.  Note this does not use the style:assemble-form() function.</p>
        <p>Type your first name in the input box. <br/>
        If you are running XForms, the output should be displayed in the output area.</p>
        <xf:input ref="PersonGivenName" incremental="true">
            <xf:label>Please enter your first name: </xf:label>
        </xf:input>
        <br/>
        <br/>
        <xf:output value="concat('Hello ', PersonGivenName, '. We hope you like XForms!')">
            <xf:label>Output: </xf:label>
        </xf:output>
        <br/>
        {if ($debug)
           then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
           else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
        }
    </body>
</html>

let $dummy := request:set-attribute("betterform.filter.ignoreResponseBody", "true")
let $xslt-pi := processing-instruction xml-stylesheet {'type="text/xsl" href="/exist/rest/db/apps/xsltforms/xsltforms.xsl"'}
let $debug-pi :=
   if ($debug)
      then processing-instruction xsltforms-options {'debug="yes"'}
      else processing-instruction xsltforms-options {'debug="no"'}
return ($xslt-pi, $debug-pi, $form)