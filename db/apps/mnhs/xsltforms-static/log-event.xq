xquery version "1.0";

let $p1 := request:get-parameter('p1', '')

let $p2 := request:get-parameter('p2', '')

let $log := util:log-system-out( concat('log-event p1=', $p1, ' p2=', $p2) )
return
<events>
   <event>{current-dateTime()}</event>
   <p1>{$p1}r</p1>
   <p2>{$p2}r</p2>
</events>