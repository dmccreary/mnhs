import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
import module namespace test-utils = "http://danmccreary.com/test-utils" at "../modules/test-utils.xqm";


let $title := 'List Unit Tests'

let $xsltforms-apps := concat($style:url-base, '/apps/XSLTForms-Demo')

let $content :=
<div class="content">

Basic XSLTForms installation sanity tests:
<a href="{$xsltforms-apps}/index.html">XSLTForms Examples</a> {' '}
<a href="{$xsltforms-apps}/modules/form.xq?form=hello.xhtml">Hello World</a>
     {test-utils:test-status('/db/apps/mnhs/xsltforms-static')}
</div>

return style:assemble-page($title, $content)