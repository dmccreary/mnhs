xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

let $debug := xs:boolean(request:get-parameter('debug', 'true'))

let $form :=
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xf="http://www.w3.org/2002/xforms">
	<head>
		<style type="text/css"><![CDATA[
     @namespace xf url("http://www.w3.org/2002/xforms");
     
    * { font-family: Ariel, Helvetica, sans-serif }
    .form {background: gray;}
    .Person {
		background: cornsilk;
    }
    
/* the labels are right-aligned in a 150px wide column */
xf|input xf|label {
   width: 150px;
   margin: 3px;
   text-align: right;
   font-weight: bold;
}

/* the input values are left aligned */
xf|value {
   text-align: left;
}

/* vertical area between input boxes */



/* each input is a row in the group table */
xf|input {
   display: inline-block;
}

/* each label within an input is a cell in the input row */
xf|input xf|label {
   display: inline-block; 
}

/* each value (pseudo-element) is also a cell in the input row */
xf|input::value {
   display: inline-block;
}
    ]]></style>
		<title>Demonstration of inserting and deleting records from a table</title>
		<xf:model id="phone-list">
			<xf:instance id="my-phone-list" src="phone-list.xml" xmlns=""/>
			<xf:submission id="update-from-local-file" method="get" action="phone-list.xml" replace="instance" instance="my-phone-list" />
			<xf:submission id="view-xml-instance" method="get" action="phone-list.xml" />
			<xf:submission id="save-to-local-file" method="put" action="phone-list.xml" />
		</xf:model>
	</head>
	<body>
		<!-- For each Person in the PersonList display the name and phone-->
		<h4>Test of Repeat with Insert and Delete Events</h4>
		
        <div class="form">
            
			<xf:repeat ref="Person" id="repeatPerson">
			   <div class="Person">
    				<xf:input ref="Name">
    					<xf:label>Name:</xf:label>
    				</xf:input>
    				
    				<xf:input ref="Phone">
    					<xf:label>Phone:</xf:label>
    				</xf:input>
    				
    				<xf:trigger>
        				<xf:label>Delete</xf:label>
        				<xf:delete ev:event="DOMActivate" ref="."/>
        			</xf:trigger>
				</div>
			</xf:repeat>
			<xf:trigger>
				<xf:label>Add Person To End</xf:label>
				<xf:action ev:event="DOMActivate">
					<xf:insert ref="/Contacts/Person[last()]" position="after" at="last()" />
					<xf:setvalue ref="/Contacts/Person[last()]/Name" value="''" />
					<xf:setvalue ref="/Contacts/Person[last()]/Phone" value="''" />
				</xf:action>
			</xf:trigger>
			<xf:trigger>
				<xf:label>Delete Selected Person</xf:label>
				<xf:delete ev:event="DOMActivate" ref="/Contacts/Person[index('repeatPerson')]"  />
			</xf:trigger>
			<br />
			<xf:submit submission="update-from-local-file">
				<xf:label>Reload</xf:label>
			</xf:submit>
			<xf:submit submission="save-to-local-file">
				<xf:label>Save</xf:label>
			</xf:submit>
			<xf:submit submission="view-xml-instance">
				<xf:label>View XML Instance</xf:label>
			</xf:submit>
        </div>
	</body>
</html>


return style:assemble-form1($form, $debug)