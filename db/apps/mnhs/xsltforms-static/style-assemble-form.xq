xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace xf="http://www.w3.org/2002/xforms";

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $title := 'Style Assemble Form Test'

let $style := <style/>

let $model :=
<xf:model>
    <xf:instance xmlns="" id="save-data">
        <data>
            <PersonGivenName/>
        </data>
    </xf:instance>
</xf:model>

let $content :=
<div class="content">
    <xf:input ref="PersonGivenName">
        <xf:label>Please enter your first name: </xf:label>
    </xf:input>
    <br/>
    <br/>
    <xf:output value="concat('Hello ', PersonGivenName, '. We hope you like XForms!')">
        <xf:label>Output: </xf:label>
    </xf:output>
    <br/>
    {if ($debug)
       then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
       else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
    }
</div>

return style:assemble-form($title, $model, $style, $content, $debug)