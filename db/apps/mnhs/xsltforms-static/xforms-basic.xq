xquery version "1.0";

import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";
declare namespace xf="http://www.w3.org/2002/xforms";

let $title := 'XForms Template'
let $dummy-attributes := ()
let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $style := ()

let $model :=
<xf:model>
    <xf:instance id="save-data">
        <data xmlns="">
            <PersonGivenName/>
        </data>
    </xf:instance>
</xf:model>

let $content :=
<div class="content">

    <xf:input ref="PersonGivenName" incremental="true">
        <xf:label>Please enter your first name: </xf:label>
    </xf:input>
    <br/>
    <xf:output ref="PersonGivenName">
        <xf:label>Hello: </xf:label>
    </xf:output>
    
    <a href="http://localhost:8080/exist/apps/XSLTForms-Demo/index.html">XSLTForms Demos</a>
</div>

(: style:assemble-form($model, $content, true()) :)
(: style:assemble-form($titleg, $dummy-attributes, $style, $model, $content, $debug) :)
return style:assemble-form($title, $model, $style, $content, $debug)

