xquery version "1.0";
import module namespace style = "http://danmccreary.com/style" at "../modules/style.xqm";

let $title := 'Submit name and id to a services for updates'

let $debug := xs:boolean(request:get-parameter('debug', 'false'))

let $form :=
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xf="http://www.w3.org/2002/xforms">
	<head>
		<style type="text/css"><![CDATA[
     @namespace xf url("http://www.w3.org/2002/xforms");
     
    body { font-family: Ariel, Helvetica, sans-serif }
    .form {background: gray;}
    .person {
		background: cornsilk;
    }
    
/* the labels are right-aligned in a 150px wide column */
xf|output xf|label {
   width: 10ex;
   margin: 10px;
   text-align: right;
   font-weight: bold;
}

/* the input values are left aligned */
xf|value {
   text-align: left;
}

/* vertical area between input boxes */



/* each input is a row in the group table */
xf|input {
   display: inline-block;
}

/* each label within an input is a cell in the input row */
xf|input xf|label {
   display: inline-block; 
}

/* each value (pseudo-element) is also a cell in the input row */
xf|input::value {
   display: inline-block;
}
    ]]></style>
		<title>{$title}</title>
		<xf:model>
			<xf:instance id="persons" src="name-id-list.xml" xmlns=""/>
			
			<xf:instance id="submit-params" xmlns="">
               <data>
                  <name>x</name>
                  <id>i</id>
              </data>
           </xf:instance>
           
			<xf:instance id="repeat-submit-result" xmlns="">
			    <empty/>
			</xf:instance>
			
			<xf:instance id="log" xmlns="">
			    <events/>
			</xf:instance>
			
			<xf:submission id="update-from-local-file" method="get" action="name-id-list.xml" replace="instance" instance="my-phone-list" />

			<xf:submission id="repeat-submit" method="get" action="update-name-id.xq" replace="instance" instance="repeat-submit-result"
			    ref="instance('submit-params')">
			    <xf:action ev:event="xforms-submit-done">
			       <xf:insert nodeset="instance('log')/event" at="last()" position="after" />
			       <xf:setvalue ref="instance('log')/event[last()]" value="'xforms-submit-done'" />
			    </xf:action>
			</xf:submission>

		</xf:model>
	</head>
	<body>
		<h4>{$title}</h4>
        <div class="form">  
			<xf:repeat ref="person" id="person-repeat">
			   <div class="person">
			        <xf:output value="position()"/>
			           
    				<xf:output ref="name">
    					<xf:label>name:</xf:label>
    				</xf:output>
    				
    				<xf:output ref="id">
    					<xf:label>id:</xf:label>
    				</xf:output>
    				
        			<xf:trigger>
        				<xf:label>Submit</xf:label>
        				<xf:action ev:event="DOMActivate">
        				   <xf:setvalue ref="instance('submit-params')/name" value="instance('persons')/person[index('person-repeat')]/name" />
        				   <xf:setvalue ref="instance('submit-params')/id" value="instance('persons')/person[index('person-repeat')]/id" />
        				   <xf:send submission="repeat-submit"/>
        				</xf:action>
        			</xf:trigger>
        			
				</div>
			</xf:repeat>

			<h5>Service Inputs</h5>
			<xf:output ref="instance('submit-params')/name">
			   <xf:label>Name=</xf:label>
			</xf:output>
			<br />
			<xf:output ref="instance('submit-params')/id">
			   <xf:label>ID=</xf:label>
			</xf:output>
			
			<h5>Service Outputs</h5>
			<xf:output ref="instance('repeat-submit-result')/name">
			   <xf:label>Name=</xf:label>
			</xf:output>
			<br />
			<xf:output ref="instance('repeat-submit-result')/id">
			   <xf:label>ID=</xf:label>
			</xf:output>
			<br />
			{if ($debug)
			   then <a href="{request:get-uri()}?debug=false">Disable Debug</a>
			   else <a href="{request:get-uri()}?debug=true">Enable Debug</a>
			}
        </div>
	</body>
</html>


return style:assemble-form1($form, $debug)