Source code repository for Library of Congress Linked Name Application using eXist 2.2.

This tool is in BETA.  We are still working on a few more issues.  We hope to also post the .xar file for other eXist users that don't want to load the application using Apache Ant.

Please contact dan@danmccreary.com if you have questions.

To get a list of build options, cd into the build directory and type "ant -p".

Dan@DAN-PC12 /d/ws
$ cd mnhs

Dan@DAN-PC12 /d/ws/mnhs (master)
$ cd build

Dan@DAN-PC12 /d/ws/mnhs/build (master)
$ ant -p
Buildfile: d:\ws\mnhs\build\build.xml
Build file for MNHS project
Main targets:

 add-execute       Make the controller.xql file executable.
 echo-properties   Check properties are set in the local.properties file.
 load-app          Load the app into the local eXist database
 reload-xsltforms  Reload XSLTForms
 xar               Build an exist package for the app xar file.
Default target: load-app

Dan@DAN-PC12 /d/ws/mnhs/build (master)

Tested on eXist 2.2